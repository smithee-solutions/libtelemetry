# Makefile for libtelemetry

# (C)Copyright 2017 Smithee Solutions LLC

TROOT=opt/tester

all:	

clean:
	rm -rf opt
	(cd libtelemetry; make clean)
	(cd collectors; make clean)

build:
	mkdir -p ${TROOT}
	(cd libtelemetry; make build)
	(cd collectors; make build)

