libtelemetry release notes for release 1.40-Build-1

   Copyright 2017 Smithee Solutions LLC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

added in 1.40 Build 1

list-cert has new option "inventory".
this makes it output a comma separated variable (csv) formatted line for the cert it reads.
there is also a script (1-zzz) in test that runs this with a list so you can check out a certificate inventory.

telemetry gets dropped in /tester/current/results

telemetry collected:
1 JSON TLS data
2 character stream output from tls connection
3 log
4 certificate from server, DER format


tls json data
symbols are in tls_client.c stuffed in the middle as a bunch
of defines.

sample:
{
"_comment" : "TLM 0.0 EP-03 (TLS)"
"start_time" : "1444516705"
"common_name" : "convergence.smithee.us"
"tls_version" : "TLS-1.2"
"key_exchange_name" : "ECDHE-RSA"
"cipher_name" : "AES-128-GCM"
"mac_name" : "AEAD"
}

punch list
parameterize dest dir
update sample


