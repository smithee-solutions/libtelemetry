/*
  Use:
    list-cert [<cert pem filename>]
    list-cert <base filename> <- or inventory> <extension with dot>

  Example:
    list-cert thing1 - .pem

  The default cert filename is cert.pem.

  (C)Copyright 2017-2018 Smithee Solutions LLC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include <gnutls/abstract.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <gnutls/compat.h>


#include <libtelemetry.h>
#include <tele_cert.h>


char
  full_public_key_hex [32668];
char
  san_inventory [16*1024];


int
  tele_extract_rsa_pubkey
    (TELE_CERT_CONTEXT
      *ctx,
    gnutls_x509_crt_t
      cert);
int
  tele_output_public_key
    (TELE_CERT_CONTEXT
      *ctx,
    gnutls_x509_crt_t
      cert,
    int
      signature_algorithm_gnutls_ident,
    char
      *key_description);
int
  tele_gnutls_sigalg_to_string
    (TELE_CERT_CONTEXT
      *ctx,
    int
      sigalg,
    char
      *sig_alg_string);


int main
  (int argc,
  char *argv [])

{ /* main for list-cert.c */

  char
    base_extension [1024];
  gnutls_x509_crt_t
    cert;
  char
    cert_contents [64*1024];
  int
    cert_format;
  time_t
    cert_time;
  TELE_CERT_CONTEXT
    context;
  char
    dn [1024];
  gnutls_datum_t
    data;
  int
    i;
  char
    issuer_dn [1024];
  size_t
    octet_count;
  int
    san_idx;
  unsigned char
    serial_octets [8192];
  int
    sil; // san inventory length
  char
    signature_algorithm [1024];
  int
    signature_algorithm_gnutls_ident;
  size_t
    size;
  int
    status;
  int
    status_io;
  int
    status_tls;
  char
    subject_dn [1024];
  char
    txt [1024];


  status = init_tele_cert (&context);
  full_public_key_hex [0] = 0;
  strcpy (base_extension, ".cer");
  strcpy (context.cert_file_base, "cert");

  if (argc > 1)
    strcpy (context.cert_file_base, argv [1]);
  if (argc > 2)
    if (0==strcmp (argv[2], "inventory"))
      context.output_format = OF_INVENTORY;
  if (argc > 3)
    strcpy (base_extension, argv [3]);
  sprintf (context.cert_filename, "%s%s", context.cert_file_base, base_extension);
  fprintf (stderr, "Base extension is %s\n", base_extension);
  fprintf (stderr,
"    certFile: %s\n",
    context.cert_filename);
  {
    FILE *f;
    f= fopen (context.cert_filename, "r");
    status_io = fread (cert_contents, sizeof (cert_contents [0]),
      sizeof (cert_contents), f);
    data.size = status_io;
    fclose (f);
    cert_format = GNUTLS_X509_FMT_PEM;
    if (0 != memcmp (cert_contents, "-----", 5))
      cert_format = GNUTLS_X509_FMT_DER;
  };
  if (context.output_format EQUALS OF_INVENTORY)
  {
    printf ("%s", context.cert_filename);
  };

  gnutls_global_init ();

  /*
    use gnutls to convert the certificate
    to a certificate structure
  */
  gnutls_x509_crt_init (&cert);
  data.data = (unsigned char *)cert_contents;
  gnutls_x509_crt_import (cert, &data, cert_format);

  status_tls = gnutls_x509_crt_get_version (cert);
  if (status_tls < 0)
  {
    fprintf (stderr, "Get Version returned error %d\n", status_tls);
    status = -1;
  };
  if (status == 0)
  {
    if (context.output_format EQUALS OF_INVENTORY)
    {
      printf (",%d", status_tls);
    }
    else
    {
      char tmp_s [1024];
      sprintf(tmp_s, 
"     Version: %d", status_tls);
      fprintf(stdout, "%s\n", tmp_s);
      status = tele_jacket_append
        (&context, "version", tmp_s);
    };

    octet_count = sizeof (serial_octets);
    status_tls = gnutls_x509_crt_get_serial (cert, serial_octets, &octet_count);
    if (status_tls < 0)
    {
      fprintf (stderr, "Get Serial returned error %d\n", status_tls);
      status = -2;
    };
  };
  if (status == 0)
  {
    if (context.output_format EQUALS OF_INVENTORY)
    {
      printf (",");
      for (i=0; i<octet_count; i++)
        printf ("%02x", serial_octets [i]);
    }
    else
    {
      printf
("      Serial: 0x");
      for (i=0; i<octet_count; i++)
        printf ("%02x", serial_octets [i]);
      printf ("\n");
    };

  };
  if (status == 0)
  {
    status_tls = gnutls_x509_crt_get_signature_algorithm (cert);
    signature_algorithm_gnutls_ident = status_tls;
    status = tele_gnutls_sigalg_to_string
      (&context, signature_algorithm_gnutls_ident, signature_algorithm);
    if (context.output_format != OF_INVENTORY)
    {
    printf
("sigAlgorithm: %s\n", signature_algorithm);
    status = tele_jacket_append
      (&context, "signature-algorithm", signature_algorithm);
    };

    /*
      get the issuer distinguished name from the cert structure.
      ...and subject
    */
  size = sizeof (dn);
  gnutls_x509_crt_get_issuer_dn (cert, dn, &size);
  strcpy(issuer_dn, dn);
  if (context.output_format EQUALS OF_INVENTORY)
  {
    printf (",\"%s\"", dn);
  }
  else
  {
    printf
("      Issuer: %s\n", dn);
  };

  cert_time = gnutls_x509_crt_get_activation_time (cert);
  if (context.output_format EQUALS OF_INVENTORY)
  {
    char line [1024];
    strcpy (line, asctime (gmtime (&cert_time)));
    line [strlen(line)-1] = 0;
    printf (",\"%s\"", line);
  }
  else
  {
    printf 
("notBeforeGMT: %s", asctime (gmtime (&cert_time)));
  };

  cert_time = gnutls_x509_crt_get_expiration_time (cert);
  if (context.output_format EQUALS OF_INVENTORY)
  {
    char line [1024];
    strcpy (line, asctime (gmtime (&cert_time)));
    line [strlen(line)-1] = 0;
    printf (",\"%s\",%012ld", line, (unsigned long int)cert_time);
  }
  else
  {
    printf 
(" notAfterGMT: %s", asctime (gmtime (&cert_time)));
  };

  size = sizeof (dn);
  gnutls_x509_crt_get_dn (cert, dn, &size);
  strcpy(subject_dn, dn);

  status = tele_jacket_append (&context, "subject-dn", subject_dn);
  status = tele_jacket_append (&context, "issuer-dn", issuer_dn);
  if (context.output_format EQUALS OF_INVENTORY)
  {
    printf (",\"%s\"", dn);
    if (strcmp(subject_dn, issuer_dn) EQUALS 0)
    {
      printf(",\"self-signed\"");
      status = tele_jacket_append (&context, "signer_status", "self-signed");
    }
    else
      printf(",\"CA-issued\"");
  }
  else
  {
    printf
("     Subject: %s\n", dn);
  };

    status = tele_output_public_key
      (&context, cert, signature_algorithm_gnutls_ident, txt);
  if (context.output_format != OF_INVENTORY)
  {
    fprintf (stderr,
" publicKey: %s",
      txt);
    fprintf (stderr,
"%s\n", full_public_key_hex);
  };

  // AIA -OCSP

{
  unsigned int critical;
  gnutls_datum_t ocsp_uri;
  char uri_string [1024];
  int status_gnutls;
  critical = 1; // don't care if it's critical or not
  memset (uri_string, 0, sizeof (uri_string));
  for (i=0; i<8; i++)
  {
    status_gnutls = gnutls_x509_crt_get_authority_info_access (cert, i, GNUTLS_IA_OCSP_URI, &ocsp_uri, &critical);
    if (status_gnutls == GNUTLS_E_SUCCESS)
    {
      memcpy (uri_string, ocsp_uri.data, ocsp_uri.size);
      fprintf (stderr, "OCSP URI %s\n", uri_string);
    };
  };
};

    fflush (stdout);
    status = tele_cert_display_extensions (&context, cert);
  };

  fflush (stdout);
  sil = sizeof (san_inventory);
  status = tele_display_subject_alt_name (&context, cert, &san_idx, san_inventory, &sil);
  fflush (stdout);

  if (context.output_format EQUALS OF_INVENTORY)
  {
    printf (",%d subjectAltName entries", san_idx);
    fflush (stdout);

      // inventory string generator adds the initial comma
    printf ("%s", tele_cert_extension_inventory (&context));
    printf (",%s", signature_algorithm);
    printf (",%s", san_inventory);
    if (context.extension_unknown > 0)
      printf (",(%d unknown extensions)", context.extension_unknown);
  };

  if (context.output_format EQUALS OF_INVENTORY)
    printf ("\n");
  fprintf (stderr, "Status returned: %d.\n", status);

  return (0);

} /* main for list-cert.c */


int
  tele_extract_rsa_pubkey
    (TELE_CERT_CONTEXT
      *ctx,
    gnutls_x509_crt_t
      cert)

{ /* tele_extract_rsa_pubkey */

  unsigned int
    bits;
  gnutls_datum_t
    e;
  char
    hextemp [1024];
  int
    i;
  char
    jacket_value [1024];
  int
    key_algorithm;
  int
    keysize;
  gnutls_datum_t
    m;
  gnutls_pubkey_t
    public_key;
  char
    public_key_type [1024];
  int
    status;
  int
    status_gnutls;


  status = ST_OK;
  bits = 0;
  status_gnutls = gnutls_pubkey_init (&public_key);
  if (status_gnutls EQUALS 0)
  {
    status_gnutls = gnutls_pubkey_import_x509 (public_key, cert, 0);
    if (status_gnutls EQUALS 0)
    {
      key_algorithm = gnutls_pubkey_get_pk_algorithm (public_key, &bits);
      printf ("guessing key algorithm: %d.\n", key_algorithm);
    };
  };
  if (status_gnutls EQUALS 0)
    status_gnutls = gnutls_x509_crt_get_pk_rsa_raw (cert, &m, &e);
  if (status_gnutls EQUALS GNUTLS_E_SUCCESS)
  {
    sprintf (public_key_type, "RSA\n");
    keysize = 8*m.size;
    if (m.data [0] EQUALS 0)
      keysize = keysize - 8;
    sprintf (hextemp, "    Key (%d bits) values:  m=", keysize);
    strcat (full_public_key_hex, hextemp);
    for (i=0; i<m.size; i++)
    {
      sprintf (hextemp, "%02x", m.data [i]);
      strcat (full_public_key_hex, hextemp);
    };
    hextemp [0] = 0;
    jacket_value [0] = 0;
    strcat (full_public_key_hex, ", e=");
    for (i=0; i<e.size; i++)
    {
      sprintf (hextemp, "%02x", e.data [i]);
      strcat (full_public_key_hex, hextemp);
      strcat (jacket_value, hextemp);
    };
    status = tele_jacket_append (ctx, "rsa-exponent", jacket_value);
    if (status EQUALS ST_OK)
    {
      sprintf (jacket_value, "%d.", bits);
      status = tele_jacket_append (ctx, "rsa-bits", jacket_value);
    };
  };
  return (status);

} /* tele_extract_rsa_pubkey */

int
  tele_guess_curve
    (TELE_CERT_CONTEXT
      *ctx,
    char
      *curve)

{

  FILE
    *cf;
  char
    command [1024];
  char
    line [1024];


  sprintf (command, "tele-sniff-curve >/tmp/curve %s",
    ctx->cert_filename);
  system (command);
  cf = fopen ("/tmp/curve", "r");
  (void) fgets (line, sizeof (line), cf);
  fclose (cf);
  if (line [strlen (line)-1] EQUALS '\n')
    line [strlen (line)-1] = 0;
  if (strlen (line) > 0)
    strcpy (curve, line);
  return (ST_OK);
}


int
  tele_output_public_key
    (TELE_CERT_CONTEXT
      *ctx,
    gnutls_x509_crt_t
      cert,
    int
      signature_algorithm_gnutls_ident,
    char
      *key_description)

{ /* tele_gnutls_display_public_key */

  unsigned int
    bits;
  gnutls_ecc_curve_t
    curve;
  char
    curve_name [1024];
  gnutls_datum_t
    e;
  char
    hextemp [1024];
  int
    i;
  char
    jacket_value [1024];
  int
    keysize;
  int
    key_algorithm;
  gnutls_datum_t
    m;
  gnutls_pubkey_t
    public_key;
  char
    public_key_type [1024];
  int
    status;
  int
    status_gnutls;
#ifdef FRESH_ECC_GNUTLS_SUPPORT
  gnutls_datum_t
    x;
  gnutls_datum_t
    y;
#endif


  status = 0;
  bits = 0;
  key_algorithm = 0;
  curve = 0;
  curve_name [0] = 0;

  if (ctx->verbosity > 3)
    fprintf (stderr, "base file: %s\n",
      ctx->cert_file_base);
  status_gnutls = gnutls_pubkey_init (&public_key);
  if (status_gnutls EQUALS 0)
  {
    status_gnutls = gnutls_pubkey_import_x509 (public_key, cert, 0);
    if (status_gnutls EQUALS 0)
    {
      key_algorithm = gnutls_pubkey_get_pk_algorithm (public_key, &bits);
      if (ctx->verbosity > 3)
      {
        fprintf (stderr, "key algorithm %d\n", key_algorithm);
      };
    };
  };
  sprintf (public_key_type, "Unidentified cert signing scheme (%d.)", signature_algorithm_gnutls_ident);
  switch (signature_algorithm_gnutls_ident)
  {
  case GNUTLS_SIGN_ECDSA_SHA1:
    strcpy (public_key_type, "ecdsa-with-SHA1 (ECC, unknown curve)");
#ifdef FRESH_ECC_GNUTLS_SUPPORT
    status_gnutls = gnutls_x509_crt_get_pk_ecc_raw (cert, &curve, &x, &y);
#else
    status_gnutls = -1;
#endif
    if (status_gnutls != GNUTLS_E_SUCCESS)
    {
      printf (
"ERROR: gnutls_x509_crt_get_pk_ecc_raw failed, error code was %d\n", status_gnutls);
      status = tele_guess_curve (ctx, curve_name);
      strcat (curve_name, "-?-");
      sprintf (public_key_type, "ECC, curve might be %s",
        curve_name);
    };
    if (status_gnutls EQUALS GNUTLS_E_SUCCESS)
    {
      strcpy (curve_name, gnutls_ecc_curve_get_name (curve));
      sprintf (public_key_type, "ECC, curve %s",
        curve_name);
    };
    break;

  case GNUTLS_SIGN_ECDSA_SHA256:
    strcpy (public_key_type, "ecdsa-with-SHA256 (ECC, unknown curve)");
#ifdef FRESH_ECC_GNUTLS_SUPPORT
    status_gnutls = gnutls_x509_crt_get_pk_ecc_raw (cert, &curve, &x, &y);
#else
    status_gnutls = -1;
#endif
    if (status_gnutls != GNUTLS_E_SUCCESS)
    {
      printf (
"ERROR: gnutls_x509_crt_get_pk_ecc_raw failed, error code was %d, message:\n  %s\n",
        status_gnutls, gnutls_strerror (status_gnutls));
      status = tele_guess_curve (ctx, curve_name);
      strcat (curve_name, "-?-");
      sprintf (public_key_type, "ECC, curve might be %s",
        curve_name);
    };
    if (status_gnutls EQUALS GNUTLS_E_SUCCESS)
    {
      strcpy (curve_name, gnutls_ecc_curve_get_name (curve));
      sprintf (public_key_type, "ECC, curve %s",
        curve_name);
    };
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
      printf (",(ECDSA...),");
    };
    break;

  case GNUTLS_SIGN_ECDSA_SHA384:
    strcpy (public_key_type, "ecdsa-with-SHA384 (ECC, unknown curve)");
#ifdef FRESH_ECC_GNUTLS_SUPPORT
    status_gnutls = gnutls_x509_crt_get_pk_ecc_raw (cert, &curve, &x, &y);
#else
    status_gnutls = -1;
#endif
    if (status_gnutls != GNUTLS_E_SUCCESS)
    {
      printf (
"ERROR: gnutls_x509_crt_get_pk_ecc_raw failed, error code was %d, message:\n  %s\n",
        status_gnutls, gnutls_strerror (status_gnutls));
      status = tele_guess_curve (ctx, curve_name);
      strcat (curve_name, "-?-");
      sprintf (public_key_type, "ECC, curve might be %s",
        curve_name);
    };
    if (status_gnutls EQUALS GNUTLS_E_SUCCESS)
    {
      strcpy (curve_name, gnutls_ecc_curve_get_name (curve));
      sprintf (public_key_type, "ECC, curve %s",
        curve_name);
    };
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
      printf (",(ECDSA...),");
    };
    break;

  case GNUTLS_SIGN_ECDSA_SHA512:
    strcpy (public_key_type, "ecdsa-with-SHA512 (ECC, unknown curve)");
#ifdef FRESH_ECC_GNUTLS_SUPPORT
    status_gnutls = gnutls_x509_crt_get_pk_ecc_raw (cert, &curve, &x, &y);
#else
    status_gnutls = -1;
#endif
    if (status_gnutls != GNUTLS_E_SUCCESS)
    {
      printf (
"ERROR: gnutls_x509_crt_get_pk_ecc_raw failed, error code was %d, message:\n%s\n",
        status_gnutls, gnutls_strerror (status_gnutls));
      strcat (curve_name, "-?-");
      sprintf (public_key_type, "ECC, curve might be %s",
        curve_name);
    };
    if (status_gnutls EQUALS GNUTLS_E_SUCCESS)
    {
      strcpy (curve_name, gnutls_ecc_curve_get_name (curve));
      sprintf (public_key_type, "ECC, curve %s",
        curve_name);
    };
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
      printf (",(ECDSA...),");
    };
    break;

  case GNUTLS_SIGN_RSA_MD2:
  case GNUTLS_SIGN_RSA_MD5:
  case GNUTLS_SIGN_RSA_SHA1:
  case GNUTLS_SIGN_RSA_SHA256:
  case GNUTLS_SIGN_RSA_SHA384:
  case GNUTLS_SIGN_RSA_SHA512:
    status_gnutls = gnutls_x509_crt_get_pk_rsa_raw (cert, &m, &e);
    if (status_gnutls EQUALS GNUTLS_E_SUCCESS)
    {
      sprintf (public_key_type, "RSA");
      keysize = 8*m.size;
      if (m.data [0] EQUALS 0)
        keysize = keysize - 8;
      sprintf (hextemp, "    Key (%d bits) values:  m=", keysize);
      strcat (full_public_key_hex, hextemp);
      for (i=0; i<m.size; i++)
      {
        sprintf (hextemp, "%02x", m.data [i]);
        strcat (full_public_key_hex, hextemp);
      };
      hextemp [0] = 0;
      jacket_value [0] = 0;
      strcat (full_public_key_hex, ", e=");
      for (i=0; i<e.size; i++)
      {
        sprintf (hextemp, "%02x", e.data [i]);
        strcat (full_public_key_hex, hextemp);
        strcat (jacket_value, hextemp);
      };
      status = tele_jacket_append (ctx, "rsa-exponent", jacket_value);
      if (status EQUALS ST_OK)
      {
        sprintf (jacket_value, "%d.", bits);
        status = tele_jacket_append (ctx, "rsa-bits", jacket_value);
      };
    };
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
      char first_8_public_key [1024];
      full_public_key_hex [0] = 0;
      for (i=0; i<m.size; i++)
      {
        sprintf (hextemp, "%02x", m.data [i]);
        strcat (full_public_key_hex, hextemp);
      };
      strcpy (first_8_public_key, full_public_key_hex);
      strcpy (first_8_public_key+16, "...");
      printf (",%s,%s", public_key_type, first_8_public_key);
    };
    break;

  default:
    strcat (public_key_type, "\n");
    printf ("***unknown certificate signature scheme (%d)\n",
      signature_algorithm_gnutls_ident);
    // try RSA anyway so we can attempt to record the key in the jacket
    status = tele_extract_rsa_pubkey (ctx, cert);
    break;
  };
  strcpy (key_description, public_key_type);
  if (strlen (curve_name) > 0)
    status = tele_jacket_append (ctx, "ecc-curve", curve_name);
  return (status);

} /* tele_gnutls_display_public_key */


int
  tele_gnutls_sigalg_to_string
    (TELE_CERT_CONTEXT
      *ctx,
    int
      sigalg,
    char
      *sig_alg_string)

{ /* tele_gnutls_sigalg_to_string */

  char
    sig_alg_s [1024];


  switch (sigalg)
  {
  case GNUTLS_SIGN_ECDSA_SHA1:
    strcpy (sig_alg_s, "ecdsa-with-SHA1");
    break;
  case GNUTLS_SIGN_ECDSA_SHA256:
    strcpy (sig_alg_s, "ecdsa-with-SHA256");
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
                        // sha1 sha256 sha384 sha512 ecdsa256 ecdsa384
      strcat (sig_alg_s, ",0,   0,     0,     0,     1,       0");
    };
    break;
  case GNUTLS_SIGN_ECDSA_SHA384:
    strcpy (sig_alg_s, "ecdsa-with-SHA384");
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
                        // sha1 sha256 sha384 sha512 ecdsa256 ecdsa384
      strcat (sig_alg_s, ",0,   0,     0,     0,     0,       1");
    };
    break;
  case GNUTLS_SIGN_ECDSA_SHA512:
    strcpy (sig_alg_s, "ecdsa-with-SHA512");
    break;
  case GNUTLS_SIGN_RSA_MD2:
    strcpy (sig_alg_s, "md2WithRSAEncryption");
    break;
  case GNUTLS_SIGN_RSA_MD5:
    strcpy (sig_alg_s, "md5WithRSAEncryption");
    break;
  case GNUTLS_SIGN_RSA_SHA1:
    strcpy (sig_alg_s, "sha1WithRSAEncryption");
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
                        // sha1 sha256 sha384 sha512 ecdsa256 ecdsa384
      strcat (sig_alg_s, ",1,   0,     0,     0,     0,       0");
    };

    break;
  case GNUTLS_SIGN_RSA_SHA256:
    strcpy (sig_alg_s, "sha256WithRSAEncryption");
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
                        // sha1 sha256 sha384 sha512 ecdsa256 ecdsa384
      strcat (sig_alg_s, ",0,   1,     0,     0,     0,       0");
    };
    break;
  case GNUTLS_SIGN_RSA_SHA384:
    strcpy (sig_alg_s, "sha384WithRSAEncryption");
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
                        // sha1 sha256 sha384 sha512 ecdsa256 ecdsa384
      strcat (sig_alg_s, ",0,   0,     1,     0,     0,       0");
    };
    break;
  case GNUTLS_SIGN_RSA_SHA512:
    strcpy (sig_alg_s, "sha512WithRSAEncryption");
    if (ctx->output_format EQUALS OF_INVENTORY)
    {
                        // sha1 sha256 sha384 sha512 ecdsa256 ecdsa384
      strcat (sig_alg_s, ",0,   0,     0,     1,     0,       0");
    };
    break;
  default:
    sprintf (sig_alg_s, "Unknown Signature Algorithm -%d.-", sigalg);
    break;
  };
  strcpy (sig_alg_string, sig_alg_s);

  return (0);

} /* tele_gnutls_sigalg_to_string */

