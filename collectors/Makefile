# Makefile for libtelemetry and friends

#  libtelemetry - network telemetry tools and support

#  Copyright 2017 Smithee Solutions LLC

#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


TROOT=opt/tester
PROGS=analyze_nmap_xml ana_tls collect_nmap collect-tls \
  get_name_from_address \
  jacket_parameter list-cert update_jacket
SCRIPTS=tele-sniff-curve
LIBTELEMETRY=../libtelemetry/libtelemetry.a
INC=../include
###  -D FRESH_ECC_GNUTLS_SUPPORT
CC=clang
LINK=clang
CFLAGS=-c -g -I ${INC} -I /${TROOT}/include -Wall -Werror
LDFLAGS=-L/opt/tester/lib -lgnutls -ltasn1 -ljansson

BUILD_TARGET=../${TROOT}

all:	${PROGS} ${SCRIPTS}

build:	all
	(cd ../libtelemetry; make build)
	mkdir -p ${BUILD_TARGET}/bin
	cp ${PROGS} ${BUILD_TARGET}/bin
	cp ${SCRIPTS} ${BUILD_TARGET}/bin

clean:
	rm -rvf ${PROGS} *.o core
	rm -rvf tls_client

ana_tls:	ana_tls.o ${LIBTELEMETRY} Makefile
	${CC} -o ana_tls -g ana_tls.o -L../libtelemetry -L/${TROOT}/lib \
	  -ltelemetry -ljansson

ana_tls.o:	ana_tls.c ${INC}/libtelemetry.h
	${CC} ${CFLAGS} ana_tls.c

analyze_nmap_xml:	analyze_nmap_xml.o ${LIBTELEMETRY} Makefile
	${CC} -o analyze_nmap_xml -g analyze_nmap_xml.o ${LIBTELEMETRY} \
	  -L/${TROOT}/lib -ljansson

analyze_nmap_xml.o:	analyze_nmap_xml.c
	${CC} ${CFLAGS} analyze_nmap_xml.c

cert_output.o:	cert_output.c ../include/tele_cert.h
	${CC} ${CFLAGS} cert_output.c

get_name_from_address:	get_name_from_address.o Makefile
	${CC} -o get_name_from_address -g get_name_from_address.o

get_name_from_address.o:	get_name_from_address.c
	${CC} -c -g -Wall -Werror -I/${TROOT}/include \
	  get_name_from_address.c

jacket_parameter:	jacket_parameter.o ${LIBTELEMETRY} Makefile
	${CC} -o jacket_parameter -g jacket_parameter.o ${LIBTELEMETRY} \
	  -L/${TROOT}/lib -ljansson

jacket_parameter.o:	jacket_parameter.c
	${CC} ${CFLAGS} -I${INC} jacket_parameter.c

list-cert:	list-cert.o cert_output.o Makefile
	${LINK} -o list-cert -g list-cert.o cert_output.o \
	  ${LDFLAGS}

list-cert.o:	list-cert.c
	${CC} ${CFLAGS} list-cert.c

update_jacket:	update_jacket.o ${LIBTELEMETRY} Makefile
	${CC} -o update_jacket -g update_jacket.o ${LIBTELEMETRY} \
	  ${LDFLAGS} -ljansson

update_jacket.o:	update_jacket.c
	${CC} ${CFLAGS} update_jacket.c

collect_nmap:	collect_nmap.o Makefile
	${CC} -o collect_nmap -g collect_nmap.o -lexpat

collect_nmap.o:	collect_nmap.c
	${CC} ${CFLAGS} collect_nmap.c

collect-tls:	collect-tls.o ${LIBTELEMETRY} Makefile tls_client.o tcp.o ex-x509-info.o
	${LINK} -o collect-tls -g collect-tls.o  \
	  tls_client.o tcp.o ex-x509-info.o \
	  -L../libtelemetry -ltelemetry ${LDFLAGS}

collect-tls.o:	collect-tls.c ${INC}/libtelemetry.h
	${CC} ${CFLAGS} collect-tls.c

tcp.o:	tcp.c
	${CC} ${CFLAGS} tcp.c 

${LIBTELEMETRY}:
	(cd ../libtelemetry; make)

ex-x509-info.o:	ex-x509-info.c ${INC}/libtelemetry.h
	${CC} ${CFLAGS} ex-x509-info.c 

tls_client.o:	tls_client.c
	${CC} ${CFLAGS} tls_client.c 

