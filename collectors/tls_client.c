/*
  tls glue code transliterated from gnutls samples.

  (C)Copyright 2017 Smithee Solutions LLC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#define GNUTLS_RANDOM_SIZE 32
#define GNUTLS_MASTER_SIZE 48
/* Note that the security parameters structure is set up after the
 * handshake has finished. The only value you may depend on while
 * the handshake is in progress is the cipher suite value.
 */
typedef struct {
	unsigned int entity;	/* GNUTLS_SERVER or GNUTLS_CLIENT */
	gnutls_kx_algorithm_t kx_algorithm;

	/* The epoch used to read and write */
	uint16_t epoch_read;
	uint16_t epoch_write;

	/* The epoch that the next handshake will initialize. */
	uint16_t epoch_next;

	/* The epoch at index 0 of record_parameters. */
	uint16_t epoch_min;

	/* this is the ciphersuite we are going to use 
	 * moved here from internals in order to be restored
	 * on resume;
	 */
	uint8_t cipher_suite[2];
	gnutls_compression_method_t compression_method;
	uint8_t master_secret[GNUTLS_MASTER_SIZE];
	uint8_t client_random[GNUTLS_RANDOM_SIZE];
	uint8_t server_random[GNUTLS_RANDOM_SIZE];
	uint8_t session_id[GNUTLS_MAX_SESSION_ID];
	uint8_t session_id_size;
	time_t timestamp;

	/* The send size is the one requested by the programmer.
	 * The recv size is the one negotiated with the peer.
	 */
	uint16_t max_record_send_size;
	uint16_t max_record_recv_size;
	/* holds the negotiated certificate type */
	gnutls_certificate_type_t cert_type;
	gnutls_ecc_curve_t ecc_curve;	/* holds the first supported ECC curve requested by client */

	/* Holds the signature algorithm used in this session - If any */
	gnutls_sign_algorithm_t server_sign_algo;
	gnutls_sign_algorithm_t client_sign_algo;

	/* Whether the master secret negotiation will be according to
	 * draft-ietf-tls-session-hash-01
	 */
	uint8_t ext_master_secret;
	/* encrypt-then-mac -> rfc7366 */
	uint8_t etm;

	/* Note: if you add anything in Security_Parameters struct, then
	 * also modify CPY_COMMON in gnutls_constate.c, and gnutls_session_pack.c,
	 * in order to save it in the session storage.
	 */

	/* Used by extensions that enable supplemental data: Which ones
	 * do that? Do they belong in security parameters?
	 */
	int do_recv_supplemental, do_send_supplemental;
	void * pversion; //const version_entry_st *pversion;
} security_parameters_st;

//#define TLM_PARAM_DEFINE
#include <libtelemetry.h>
extern FILE *tlmlog;


extern TLM_STREAM
  recorder_1;
extern TLM_STREAM
  recorder_2;


/* A very basic TLS client, with X.509 authentication and server certificate
 * verification. Note that error checking for missing files etc. is omitted
 * for simplicity.
 */

#define MAX_BUF 1024
#define CAFILE "etc/ca-certificates.pem"
#define MSG "GET / HTTP/1.0\r\n\r\n"

extern int tcp_connect(void);
extern void tcp_close(int sd);
int _verify_certificate_callback(gnutls_session_t session);

int
  tls_client
    (TLM_CONTEXT
      *tctx)

{ /* tls_client */

  int ret, sd, ii;
  gnutls_session_t session;
  char buffer[MAX_BUF + 1];
  const char
    *err;
  gnutls_certificate_credentials_t
    xcred;
  int
    status;
  char
    filename [1024];


  status = ST_OK;
  xcred = NULL;
  sd = -1;
  session = 0;

  if (status EQUALS ST_OK)
  {
    if (gnutls_check_version("3.1.4") == NULL) {
      fprintf(stderr, "GnuTLS 3.1.4 or later is required for this example\n");
      exit(1);
    }

    /* for backwards compatibility with gnutls < 3.3.0 */
    gnutls_global_init();

    /* X509 stuff */
    gnutls_certificate_allocate_credentials(&xcred);

    /* sets the trusted cas file
    */
    sprintf (filename, "/%s/%s", TROOT, CAFILE);
    gnutls_certificate_set_x509_trust_file(xcred, filename,
      GNUTLS_X509_FMT_PEM);
    if (param_verify != 0)
      gnutls_certificate_set_verify_function (xcred, _verify_certificate_callback);

    /* If client holds a certificate it can be set using the following:
     *
       gnutls_certificate_set_x509_key_file (xcred, 
         "cert.pem", "key.pem", 
         GNUTLS_X509_FMT_PEM); 
    */

        /* Initialize TLS session 
         */
        gnutls_init(&session, GNUTLS_CLIENT);

        gnutls_session_set_ptr(session, (void *) tctx->server_fqdn);

        gnutls_server_name_set(session, GNUTLS_NAME_DNS, param_target1_fqdn,
                               strlen(param_target1_fqdn));

        /* use default priorities */
        gnutls_set_default_priority(session);

// WARNING WARNING WARNING this enables SSL-3 for DIAGNOSTIC PURPOSES
// ONLY

// use fine-grained
if (0)
  fprintf (stderr, "Parameter PRIORITY is %s\n", param_priority);
	/*
          if more fine-grained control is required
        */
        ret = gnutls_priority_set_direct (session, param_priority, &err); //"LEGACY:+VERS-SSL3.0", &err);
        if (ret < 0)
        {
          if (ret == GNUTLS_E_INVALID_REQUEST)
          {
            fprintf (tlmlog, "Syntax error at: %s\n", err);
          }
          exit(1);
        }

        /* put the x509 credentials to the current session
         */
        gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred);

        /* connect to the peer
         */
        sd = tcp_connect();

        gnutls_transport_set_int(session, sd);
        gnutls_handshake_set_timeout(session,
                                     GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

        /* Perform the TLS handshake
         */
        do {
                ret = gnutls_handshake(session);
        }
        while (ret < 0 && gnutls_error_is_fatal(ret) == 0);

        if (ret < 0)
        {
          fprintf (tlmlog,
            "*** Handshake failed\n");
          fprintf (tlmlog, "gnutls error: %d (", ret);
switch (ret)
{
case GNUTLS_E_CERTIFICATE_ERROR:
  fprintf (tlmlog, "gnutls Err Error in the certificate.");
  break;
case GNUTLS_E_FATAL_ALERT_RECEIVED:
  fprintf (tlmlog, "gnutls Err Fatal Alert Received");
  break;
case GNUTLS_E_UNSUPPORTED_VERSION_PACKET:
  fprintf (tlmlog, "gnutls Err Unsupported Version Packet");
  break;
default:
  fprintf (tlmlog, "Unknown");
  break;
};
fprintf (tlmlog, ")\n");

          gnutls_perror(ret); printf ("\n");
          goto end;
        }
        else
        {

{
  int i;
  char ver [1024];

  i = gnutls_protocol_get_version (session);
  switch (i)
  {
  case GNUTLS_SSL3:
    strcpy (ver, "SSL-3");
    break;
  case GNUTLS_TLS1_0:
    strcpy (ver, "TLS-1.0");
    break;
  case GNUTLS_TLS1_1:
    strcpy (ver, "TLS-1.1");
    break;
  case GNUTLS_TLS1_2:
    strcpy (ver, "TLS-1.2");
    break;
  default:
    strcpy (ver, "unknown");
    break;
  };
  strcpy (recorder_1.tls_telemetry.tls_version, ver);
};
{
  const char *kx_name;
security_parameters_st *p;
kx_name = "?";
p = (security_parameters_st *)session;
  kx_name = gnutls_kx_get_name (p->kx_algorithm);
  if (!kx_name)
    fprintf (stderr, "unknown algorithm in tls_client\n");
  else
  {
    strcpy (recorder_1.tls_telemetry.key_exchange_name, kx_name);
  };
  strcpy (recorder_1.tls_telemetry.cipher_name,
    gnutls_cipher_get_name (gnutls_cipher_get (session)));
  strcpy (recorder_1.tls_telemetry.mac_name,
    gnutls_mac_get_name (gnutls_mac_get (session)));
};

print_x509_certificate_info (tctx, session);

        }

        gnutls_record_send(session, MSG, strlen(MSG));

        ret = gnutls_record_recv(session, buffer, MAX_BUF);
        if (ret == 0) {
                printf("- Peer has closed the TLS connection\n");
                goto end;
        } else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) {
                fprintf(stderr, "*** Warning: %s\n", gnutls_strerror(ret));
        } else if (ret < 0) {
                fprintf(stderr, "*** Error: %s\n", gnutls_strerror(ret));
                goto end;
        }

        if (ret > 0)
        {
          for (ii = 0; ii < ret; ii++)
          {
            fprintf (recorder_2.file,
              "%c", buffer[ii]);
          };
        };

        gnutls_bye(session, GNUTLS_SHUT_RDWR);
  };

      end:

        tcp_close(sd);

        gnutls_deinit(session);

        gnutls_certificate_free_credentials(xcred);

        gnutls_global_deinit();

        return 0;
}

/* This function will verify the peer's certificate, and check
 * if the hostname matches, as well as the activation, expiration dates.
 */
int _verify_certificate_callback(gnutls_session_t session)
{
        unsigned int status;
        int ret, type;
        const char *hostname;
        gnutls_datum_t out;

        /* read hostname */
        hostname = gnutls_session_get_ptr(session);

        /* This verification function uses the trusted CAs in the credentials
         * structure. So you must have installed one or more CA certificates.
         */

         /* The following demonstrate two different verification functions,
          * the more flexible gnutls_certificate_verify_peers(), as well
          * as the old gnutls_certificate_verify_peers3(). */
#if 1
        {
        gnutls_typed_vdata_st data[2];

        memset(data, 0, sizeof(data));

        data[0].type = GNUTLS_DT_DNS_HOSTNAME;
        data[0].data = (void*)hostname;

        data[1].type = GNUTLS_DT_KEY_PURPOSE_OID;
        data[1].data = (void*)GNUTLS_KP_TLS_WWW_SERVER;

        ret = gnutls_certificate_verify_peers(session, data, 2,
					      &status);
        }
#else
        ret = gnutls_certificate_verify_peers3(session, hostname,
					       &status);
#endif
        if (ret < 0) {
                printf("Error\n");
                return GNUTLS_E_CERTIFICATE_ERROR;
        }

        type = gnutls_certificate_type_get(session);

        ret =
            gnutls_certificate_verification_status_print(status, type,
                                                         &out, 0);
        if (ret < 0) {
                printf("Error\n");
                return GNUTLS_E_CERTIFICATE_ERROR;
        }

        printf("%s", out.data);

        gnutls_free(out.data);

        if (status != 0)        /* Certificate is not trusted */
                return GNUTLS_E_CERTIFICATE_ERROR;

        /* notify gnutls to continue handshake normally */
        return 0;
}
