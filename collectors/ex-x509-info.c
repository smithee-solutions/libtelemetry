/*
  tls glue code transliterated from gnutls samples.

  (C)Copyright 2017 Smithee Solutions LLC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#include <libtelemetry.h>
extern TLM_STREAM
  recorder_1;
extern TLM_STREAM
  recorder_4;

static const char *bin2hex(const void *bin, size_t bin_size)
{
        static char printable[110];
        const unsigned char *_bin = bin;
        char *print;
        size_t i;

        if (bin_size > 50)
                bin_size = 50;

        print = printable;
        for (i = 0; i < bin_size; i++) {
                sprintf(print, "%.2x ", _bin[i]);
                print += 2;
        }

        return printable;
}

/* This function will print information about this session's peer
 * certificate.
 */
void print_x509_certificate_info
  (TLM_CONTEXT *tctx,
  gnutls_session_t session)
{
        const gnutls_datum_t *cert_list;
        unsigned int cert_list_size = 0;
        gnutls_x509_crt_t cert;

        /* This function only works for X.509 certificates.
         */
        if (gnutls_certificate_type_get(session) != GNUTLS_CRT_X509)
                return;

        cert_list = gnutls_certificate_get_peers(session, &cert_list_size);

// cert_list_size is number of certificates peer provided
//        printf("Peer provided %d certificates.\n", cert_list_size);

        if (cert_list_size > 0) {

                /* we only print information about the first certificate.
                 */
                gnutls_x509_crt_init(&cert);

                gnutls_x509_crt_import(cert, &cert_list[0],
                                       GNUTLS_X509_FMT_DER);
{
  unsigned char *cert;
  cert = (cert_list [0].data);
  fwrite (cert, sizeof (unsigned char), cert_list [0].size,
    recorder_4.file);
};
{
  unsigned int ca;
  gnutls_x509_dn_t distinguished_name;
  gnutls_x509_ava_st ava;
  int irdn;
  int iava;
  int status;
  int done;

  irdn = 0;
  iava = 0;

  done = 0;
  status = gnutls_x509_crt_get_subject
    (cert, &distinguished_name);
  if (status != 0)
  {
    printf ("get subject %d\n", status);
    done = 1;
  };
  while (!done)
  {
    if (!done)
    {
      status = gnutls_x509_dn_get_rdn_ava
        (distinguished_name, irdn, iava, &ava);
      if (status != 0)
        done = 1;
      else
      {
        char object_identifier [1024];
        char value [1024];
        char oid_name [1024];
        memset (object_identifier, 0, sizeof (object_identifier));
        memcpy (object_identifier,
          ava.oid.data, ava.oid.size);
        memset (value, 0, sizeof (value));
        memcpy (value,
          ava.value.data, ava.value.size);

        strcpy (oid_name, object_identifier);
        if (tctx->verbosity > 3)
          fprintf (stderr, "oid detected: %s\n",
            object_identifier);
        if (strcmp ("2.5.4.3", object_identifier) EQUALS 0)
        {
          strcpy (recorder_1.tls_telemetry.subj_common_name,
            value);
          strcpy (oid_name, "CN");
        };
        // printf ("%s=%s\n", oid_name, value);
        irdn ++;
      };
    };
  };

  // yes this repeats the above code and should be in a function.

  done = 0;
  irdn = 0;
  iava = 0;
  status = gnutls_x509_crt_get_issuer
    (cert, &distinguished_name);
  if (status != 0)
  {
    printf ("get issuer %d\n", status);
    done = 1;
  };
  while (!done)
  {
    if (!done)
    {
      status = gnutls_x509_dn_get_rdn_ava
        (distinguished_name, irdn, iava, &ava);
      if (status != 0)
        done = 1;
      else
      {
        char object_identifier [1024];
        char value [1024];
        char oid_name [1024];
        memset (object_identifier, 0, sizeof (object_identifier));
        memcpy (object_identifier,
          ava.oid.data, ava.oid.size);
        if (tctx->verbosity > 3)
          fprintf (stderr, "oid %s\n", object_identifier);
        memset (value, 0, sizeof (value));
        memcpy (value,
          ava.value.data, ava.value.size);
        strcpy (oid_name, object_identifier);
        if (strcmp ("2.5.4.3", object_identifier) EQUALS 0)
        {
          strcpy (recorder_1.tls_telemetry.issuer_common_name,
            value);
          strcpy (oid_name, "CN");
        };
        //printf ("%s=%s\n", oid_name, value);
        irdn ++;
      };
    };
  };

  recorder_1.tls_telemetry.not_before_time =
    gnutls_x509_crt_get_activation_time (cert);
  recorder_1.tls_telemetry.not_after_time =
    gnutls_x509_crt_get_expiration_time (cert);

  // basic constraints


  status = gnutls_x509_crt_get_basic_constraints(cert,
    (unsigned int *)&(recorder_1.tls_telemetry.critical),
    &ca,
    &(recorder_1.tls_telemetry.path_length));
  sprintf(recorder_1.tls_telemetry.certificate_authority, "%d", ca);

  char serial [40];
  size_t serial_size;
  serial_size = sizeof (serial);
  gnutls_x509_crt_get_serial (cert, serial, &serial_size);
  strcpy (recorder_1.tls_telemetry.serial_hex,
    bin2hex (serial, serial_size));
};

        }
}
