#define SNAP_BOUNDRY 20000
#define UPPER_LIMIT 1000000
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define EQUALS ==


int
  main
    (int argc,
    char *argv[])
{ /* main for cs-peel */

  int done;
  FILE *errlog;
  FILE *fcs;
  int index;
  char line [32*1024];
  int status;
  char *status_gets;


  status = 0;
  errlog = stderr;
  index=1;
  done=0;
  fcs = fopen (argv[1], "r");
  if (fcs EQUALS NULL)
  {
    status = -1;
    done = 1;
  };
  while (!done)
  {
    status_gets = fgets(line, sizeof(line), fcs);
    if (status_gets EQUALS NULL)
    {
      status = -2; // read error
    };
    if (sizeof(line) EQUALS strlen(line))
    {
       status = -3; // overflow
    };
    if (status != 0)
      done = 1;
    if (strlen(line) > 0)
    {
char sample_name_base[1024];
char sample_filename[1024];
FILE *fjson;
char command [1024];
      sprintf(sample_name_base, "%08d", index);
      sprintf(sample_filename, "%s.json", sample_name_base);
      fjson = fopen(sample_filename, "w");
      fprintf(fjson, "%s", line);
      fclose(fjson);
      //fprintf(errlog, "Sample %d Line: %s\n", index, line);
      sprintf(command, "cs-extract-cert %s", sample_name_base);
      system(command);
if (0 EQUALS (index % SNAP_BOUNDRY))
{
  sprintf(command, "x-snap %09d", index);
  system (command);
};
if (index > UPPER_LIMIT)
  done = 1; // stop at upper limit
    };
    if (!done)
      index ++;
  };
  return (status);

} /* main for cs-peel */

