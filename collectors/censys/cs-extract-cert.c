int verbosity=1;

#include <memory.h>


#include <PCSC/wintypes.h>
#include <PCSC/pcsclite.h>
#include <PCSC/winscard.h>


#include <jansson.h>

#define EQUALS ==


int
  main
    (int argc,
    char *argv [])

{ /* main for thing1 */

  char base64_filename [1024];
  char base64_string [32*1024];
  char command[1024];
  FILE *errlog;
  FILE *fbase64;
  char json_string [32*1024];
  FILE *log;
  char parameter [1024]; // for json parsing
  char parameter_filename [1024]; // the json input file's name
  FILE *paramf;
  json_t *root;
  char sample_filename_base [1024];
  int status;
  int status_io;
  json_error_t status_json;
  json_t *value1;
  json_t *value2;
  json_t *value3;
  json_t *value4;
  json_t *value5;


  status = 0;
  log = stdout;
  errlog = stderr;

  if (argc > 1)
    strcpy (sample_filename_base, argv [1]);
  else
    strcpy (sample_filename_base, "sample.json");

  if (verbosity > 1)
    fprintf(errlog, "cs-extract-cert 0.00.0 EP-02\n");
  sprintf(parameter_filename, "%s.json", sample_filename_base);
  paramf = fopen(parameter_filename, "r");
  if (paramf != NULL) {
    memset(json_string, 0, sizeof(json_string));
    status_io =
      fread(json_string, sizeof(json_string[0]), sizeof(json_string), paramf);
    if (status_io >= sizeof(json_string))
      status = -1;
    if (status_io <= 0)
      status = -1;
  };
  if (status EQUALS 0) {
    root = json_loads(json_string, 0, &status_json);
    if (!root) {
      status = -2;
      fprintf(errlog, "bad json string: %s\n", json_string);
    };

    // outer tag is 'data'
    
//data tls server_certificates certificates raw    
    strcpy(parameter, "data");
    value1 = json_object_get(root, parameter);
    if (value1)
    {
      if (verbosity > 3)
        fprintf(stderr, "...found data\n");
      strcpy(parameter, "tls");
      value2 = json_object_get(value1, parameter);
      if (value2)
      {
        if (verbosity > 3)
          fprintf(stderr, "...found tls\n");
        strcpy(parameter, "server_certificates");
        value3 = json_object_get(value2, parameter);
        if (value3)
        {
          if (verbosity > 3)
            fprintf(stderr, "...found server_certificates\n");
          strcpy(parameter, "certificate");
          value4 = json_object_get(value3, parameter);
          if (value4)
          {
            if (verbosity > 3)
              fprintf(stderr, "...found certificate\n");
            strcpy(parameter, "raw");
            value5 = json_object_get(value4, parameter);
            if (value5)
            {
	      strcpy (base64_string, json_string_value(value5));
              if (verbosity > 3)
                fprintf(errlog, "base 64 string is %ld characters\n", strlen(base64_string));
              //fprintf(stderr, "CERTIFICATE:\n  %s\n", json_string_value(value5));
              if (verbosity > 1)
                fprintf(log, "Certificate extracted from %s.json\n",
                sample_filename_base);
	      sprintf(base64_filename, "%s.base64", sample_filename_base);
	      fbase64 = fopen(base64_filename, "w");
	      fprintf(fbase64, "%s", base64_string);
	      fclose(fbase64);
	      sprintf(command, "x-decode-cert %s",
                sample_filename_base);
              if (verbosity > 9)
              {
                fprintf(errlog, "sample filename base: %s\n", sample_filename_base);
                fprintf(errlog, "base64 filename: %s\n", base64_filename);
                fprintf(errlog, "decode command: %s\n", command);
              };
	      system(command);
            };
          };
        };
      };
    };
  };

  if (status != 0)
  {
    fprintf(stderr, "Status was %d\n", status);
  };
  return (status);

} /* main for thing1 */

#if 0
data
in that tls
in that server_certificates
in that certificates
in that raw

#endif

