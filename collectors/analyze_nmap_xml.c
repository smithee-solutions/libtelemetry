// default input nmap_output.json
// assumes max 32kbyte nmap json (converted xml) file

#include <stdio.h>
#include <string.h>


#include <jansson.h>
#include <gnutls/gnutls.h>


#include <libtelemetry.h>


char
  details [1024];
char
  entry_name [1024];
TLM_JACKET
  jacket;
char
  spreadsheet_string [1024];
int
  verbosity = 1;


int
  extract_ipv4_address
    (json_t
      *addr_list_entry)

{ /* extract_ipv4_address */

  json_t
    *address_type;
  char
    address_type_s [1024];
  json_t
    *address_value;
  char
    address_value_s [1024];
  json_t
    *vendor_value;
  char
    vendor_value_s [1024];
  char
    field [1024];
  int
    status;


  status = ST_OK;
  if (status EQUALS ST_OK)
  {
    // fetch "@addrtype" field

    strcpy (field, "@addrtype");
    address_type = json_object_get (addr_list_entry, field);
   if (address_type EQUALS NULL)
     status = -1;
  };
  if (status EQUALS ST_OK)
  {
    strcpy (address_type_s, json_string_value (address_type));
    strcpy (field, "@addr");
    address_value = json_object_get (addr_list_entry, field);
    if (address_value EQUALS NULL)
status = -1;
  };
  if (status EQUALS ST_OK)
  {
    strcpy (address_value_s, json_string_value (address_value));
    if (strcmp (address_type_s, "ipv4") == 0)
    {
      strcpy (entry_name, address_value_s); // use ipv4addr as name
      sprintf (details, " \"ipv4\" : \"%s\",\n", address_value_s);
      strcat (spreadsheet_string, details);
    };
            if (strcmp (address_type_s, "mac") == 0)
            {
              sprintf (details, " \"mac\" : \"%s\",", address_value_s);
              strcat (spreadsheet_string, details);

              // if it's a MAC address, get the vendor
              strcpy (field, "@vendor");
              vendor_value = json_object_get (addr_list_entry, field);
              if (vendor_value EQUALS NULL)
                status = ST_JACKET_VENDOR_ERROR;
              if (status EQUALS ST_OK)
              {
                strcpy (vendor_value_s, json_string_value (vendor_value));
                sprintf (details, " \"vendor\" : \"%s\",", vendor_value_s);
                strcat (spreadsheet_string, details);
              };
            };
          };
  return (status);

} /* extract_ipv4_address */


int
  main
    (int
      argc,
    char
      *argv [])

{ /* main for analyze_nmap_xml */

  json_t
    *addr_list_entry;
  int
    address_list_size;
  json_t
    *address_type;
  json_t
    *address_value;
  char
    address_type_s [1024];
  char
    address_value_s [1024];
  char
    collection_directory [1024];
  char
    field [1024];
  json_t
    *fqdn;
  json_t
    *host_address_list;
  json_t
    *host_entry;
  int
    host_index;
  int
    host_list_size;
  json_t
    *hostname_details;
  char
    hostname_value [1024];
  json_t
    *hostnames;
  json_t
    *hosts_array;
  char
    irep_filename [1024];
  int
    j;
  char
    json_string [512*1024];
  FILE
    *nmapf;
  json_t
    *nmaprun;
  char
    nmap_output_filename [1024];
  TLM_JACKET
    old_jacket;
  json_t
    *root;
  int
    status;
  int
    status_io;
  json_error_t
    status_json;
  char
    vendor_name_s [1024];
  json_t
    *vendor_value;
  int
    verbosity;


  status = ST_OK;
  verbosity = 1;
  strcpy (nmap_output_filename, "nmap_output.json");
  strcpy (collection_directory, "./collect");
  if (argc > 1)
    strcpy (nmap_output_filename, argv [1]);
  if (argc > 2)
    verbosity = 9;

  fprintf (stderr, "Processing NMAP XML->JSON->libtelemetry\n");
  fprintf (stderr, "                                 file %s\n", nmap_output_filename);
  fprintf (stderr, "  Output will be written to directory %s\n", collection_directory);

  nmapf = fopen (nmap_output_filename, "r");
  if (nmapf EQUALS NULL)
    status = ST_BAD_NMAP_FILE;
  if (status EQUALS ST_OK)
  {
    memset (json_string, 0, sizeof (json_string));
    status_io = fread (json_string,
      sizeof (json_string [0]), sizeof (json_string), nmapf);
    if (status_io >= sizeof (json_string))
      status = ST_CMD_OVERFLOW;
    if (status_io <= 0)
      status = ST_CMD_UNDERFLOW;
    if (status != ST_OK)
      fprintf (stderr, "after fread status %d\n", status);
  };
  if (status EQUALS ST_OK)
  {
    root = json_loads (json_string, 0, &status_json);
    if (!root)
    {
      fprintf (stderr, "JSON parser failed.  String was ->\n%s<-\n",
        json_string);
      status = ST_CMD_ERROR;
    };
  };
  if (status EQUALS ST_OK)
  {
    strcpy (field, "nmaprun");
    nmaprun = json_object_get (root, field);
    if (!json_is_object (nmaprun))
      status = ST_PARSE_NMAP_NO_NMAPRUN;
  };
  if (status EQUALS ST_OK)
  {

    if (verbosity > 1)
      fprintf (stderr, "nmaprun object detected\n");
    strcpy (field, "host");
    hosts_array = json_object_get (nmaprun, field);
    if (!json_is_array (hosts_array))
      status = ST_PARSE_NMAP_NO_HOST;
  };
  if (status EQUALS ST_OK)
  {
    if (verbosity > 3)
      fprintf (stderr, "host array detected\n");

    host_list_size = json_array_size (hosts_array);
    if (verbosity > 1)
      fprintf (stderr, "Hosts array contains %d entries\n",
        host_list_size);
    for (host_index=0; host_index < host_list_size; host_index++)
    {

      if (verbosity > 3)
        fprintf (stderr, "top of array about to get host_entry %d\n",
          host_index);
memset (spreadsheet_string, 0, sizeof (spreadsheet_string));
      memset (&jacket, 0, sizeof (jacket));
      host_entry = json_array_get (hosts_array, host_index);

      // initialize entry name to empty.  will be filled in with (ipv4 or something)
      entry_name [0] = 0;

      strcpy (field, "address");
      host_address_list = json_object_get (host_entry, field);
      if (!json_is_array (host_address_list))
      {
        if (!json_is_object (host_address_list))
          status = ST_PARSE_NMAP_HOST_ADDRESSES;
        else
        {
          /*
            it had only one entry so it parses in JSON as an object not an
            array.
          */
          if (verbosity > 1)
            fprintf (stderr, "address list had only one object\n");
          status = extract_ipv4_address (host_address_list);
        };
      };
      if (status EQUALS ST_OK)
      {
json_t *dsap_number;
char dsap_s [1024];
json_t *ports_list;
json_t *port_array;
json_t *port_list_entry;
json_t *protocol_name;
int reserved_for_tcp443;
char save_dsap_s [1024];
json_t *state;
json_t *state_info;
        port_array = NULL;
        strcpy (field, "ports");
        ports_list = json_object_get (host_entry, field);
        if(!json_is_object (ports_list))
status = -2;
        else
        {
          strcpy (field, "port");
          port_array = json_object_get (ports_list, field);
        };
        if (!json_is_array (port_array))
status = -2;
        else
        {
int dsap_index;
int i;
int list_size;
          list_size = json_array_size (port_array);
          /*
            we only save the first 3.  we make sure to save tcp/443.
          */
          reserved_for_tcp443 = 0;
          save_dsap_s [0] = 0;
          dsap_index = 0;
          for (i=0; i<list_size; i++)
          {
            port_list_entry = json_array_get (port_array, i);
            strcpy (field, "state");
            state_info = json_object_get (port_list_entry, field);
            strcpy (field, "@state");
            state = json_object_get (state_info, field);

            // record if open port

            if (0 EQUALS strcmp ("open", json_string_value (state)))
            {
              strcpy (field, "@portid");
              dsap_number = json_object_get (port_list_entry, field);
              strcpy (field, "@protocol");
              protocol_name = json_object_get (port_list_entry, field);
              sprintf (dsap_s, "%s/%s",
                json_string_value (protocol_name), json_string_value (dsap_number));
              if (verbosity > 3)
                fprintf (stderr, "new DSAP: %s\n", dsap_s);
              sprintf (details, " \"dsap_%d\" : \"%s\",\n",
                dsap_index, dsap_s);
              if (dsap_index < 2)
              {
                strcat (spreadsheet_string, details);
                dsap_index ++;
              }
              else
              {
                // if not the first 2 then save this one unless it's tcp/443 then mark it
                strcpy (save_dsap_s, dsap_s);
                if (0 EQUALS strcmp ("tcp/443", dsap_s))
                {
                  save_dsap_s [0] = 0;
                  reserved_for_tcp443 = 1;
                };
              };
            };
          };
          // after loop if saved 443 use it else use last we got
          if (reserved_for_tcp443)
          {
            sprintf (details, " \"dsap_%d\" : \"%s\",\n",
              2, "tcp/443");
            strcat (spreadsheet_string, details);
          }
          else
          {
            if (strlen (save_dsap_s) > 0)
            {
              sprintf (details, " \"dsap_%d\" : \"%s\",\n",
                2, dsap_s);
              strcat (spreadsheet_string, details);
            };
          };
        };
        status = ST_OK;
      };
      if (status EQUALS ST_OK)
      {
        if (verbosity > 3)
          fprintf (stderr, "host array object at index %d retrieved.\n",
            host_index);
        address_list_size = json_array_size (host_address_list);
        if (verbosity > 3)
          fprintf (stderr, "entry contains %d entries in address array\n",
            address_list_size);
        for (j=0; j<address_list_size; j++)
        {
          addr_list_entry = json_array_get (host_address_list, j);
          if (addr_list_entry EQUALS NULL)
status = -1;
          if (status EQUALS ST_OK)
          {
            // fetch "@addrtype" field

            strcpy (field, "@addrtype");
            address_type = json_object_get (addr_list_entry, field);
            if (address_type EQUALS NULL)
status = -1;
          };
          if (status EQUALS ST_OK)
          {
            strcpy (address_type_s, json_string_value (address_type));
            strcpy (field, "@addr");
            address_value = json_object_get (addr_list_entry, field);
            if (address_value EQUALS NULL)
status = -1;
          };
          if (status EQUALS ST_OK)
          {
            strcpy (address_value_s, json_string_value (address_value));
            if (strcmp (address_type_s, "ipv4") == 0)
            {
              strcpy (entry_name, address_value_s); // use ipv4addr as name
              sprintf (details, " \"ipv4\" : \"%s\",\n", address_value_s);
              strcat (spreadsheet_string, details);
              strcpy (jacket.ipv4, address_value_s);
            };
            if (strcmp (address_type_s, "mac") == 0)
            {
              sprintf (details, " \"mac\" : \"%s\",", address_value_s);
              strcat (spreadsheet_string, details);
              strcpy (jacket.mac, address_value_s);

              // if it's a MAC address, get the vendor
              strcpy (field, "@vendor");
              vendor_value = json_object_get (addr_list_entry, field);
              if (vendor_value EQUALS NULL)
status = -1;
              if (status EQUALS ST_OK)
              {
                strcpy (vendor_name_s, json_string_value (vendor_value));
                sprintf (details, " \"vendor\" : \"%s\",", vendor_name_s);
                strcat (spreadsheet_string, details);
                strcpy (jacket.vendor, vendor_name_s);
              };
            };
          };
        };
      };
        if (status EQUALS ST_PARSE_NMAP_HOST_ADDRESSES)
        {
          if (verbosity > 1)
            fprintf (stderr, "no addresses array found in index %d\n",
              host_index);
          status = ST_OK; // not fatal, continue
        };

      strcpy (field, "hostnames");
      hostnames = json_object_get (host_entry, field);
      if (!json_is_object (hostnames))
status = -1;
      if (status EQUALS ST_OK)
      {
        strcpy (field, "hostname");
        hostname_details = json_object_get (hostnames, field);
        if (!json_is_object (hostname_details))
          status = ST_PARSE_NMAP_HOSTNAME;
        if (status EQUALS ST_OK)
        {
          strcpy (field, "@name");
          fqdn = json_object_get (hostname_details, field);
          if (!json_is_string (fqdn))
            status = ST_PARSE_NMAP_ATNAME;
        };
        if (status EQUALS ST_OK)
        {
          strcpy (hostname_value, json_string_value (fqdn));
          if (verbosity > 1)
            fprintf (stderr, "host_index %d FQDN is %s\n", host_index, hostname_value);
          sprintf (details, " \"fqdn\" : \"%s\",",
            hostname_value);
          strcat (spreadsheet_string, details);
          strcpy (jacket.fqdn, hostname_value);

          // if we didn't have an entry name yet use this name

          if (strlen (entry_name) EQUALS 0)
            strcpy (entry_name, hostname_value);
        };

        if (status EQUALS ST_PARSE_NMAP_HOSTNAME)
        {
          if (verbosity > 3)
            fprintf (stderr, "no hostname object found, continuing\n");
          status = ST_OK; // not catastrophic, continue processing
        };
      };
      /*
        finished grabbing address info.  now go get dns lookup info.
        for NMAP xml remember this is the reported fqdn from doing a reverse on the
        address so you can't really assume this is the known fqdn of the device that
      */
      strcat (spreadsheet_string, "\"_end\" : \"generated by analyze_nmap_xml\" }\n");
      if (verbosity > 3)
        fprintf (stderr, "status %d, spreadsheet: %s\n",
          status, spreadsheet_string);

      /*
        done grabbing info about this specific detected network node.
        output a libtelemetry-style "json" file with the stuff.

        for this tool we create e..g a collect/jacket_<ipv4>.json file.

        collection_directory is created if it does not already exist.
        this assumes the tool has write access to do all this.
      */
// read current jacket
// if there was a current jacket skip this
      sprintf (irep_filename, "%s/jacket_%s.json",
        collection_directory, entry_name);
      status = read_jacket (irep_filename, &old_jacket);
      if (status EQUALS ST_BAD_JACKET)
      {
        char
          command [1024];
        FILE
          *irepf;

        status = ST_OK;
        sprintf (command, "mkdir -p %s",
          collection_directory);
        system (command);
        if (verbosity >3)
          fprintf (stderr, "writing entry to directory %s filename will be %s\n",
            collection_directory, irep_filename);
        irepf = fopen (irep_filename, "w");
        if (irepf != NULL)
        {
          if (verbosity >3)
            fprintf (stderr, "writing file now\n");
          fprintf (irepf, "{\n%s", spreadsheet_string);
          fclose (irepf);
        };
      };
    } /* for each host in list */;
  };

  if (verbosity > 1)
    if (status != ST_OK)
      fprintf (stderr, "Exit status %d\n", status);
  return (status);
} /* main for analyze_nmap_xml */

