
/* This example code is placed in the public domain. */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>

#include <gnutls/gnutls.h>

#include <libtelemetry.h>

/* tcp.c */
int tcp_connect(void);
void tcp_close(int sd);

/* Connects to the peer and returns a socket
 * descriptor.
 */
extern int tcp_connect(void)
{
  int
    err;
  int
    sd;
  struct sockaddr_in
    sa;


        /* connects to server
         */
        sd = socket(AF_INET, SOCK_STREAM, 0);

        memset(&sa, '\0', sizeof(sa));
        sa.sin_family = AF_INET;
        sa.sin_port = htons(atoi(param_dest_SAP));
{
  struct addrinfo
    hints;
  struct addrinfo
    *results;
  int
    status_net;
struct sockaddr * p1;
struct sockaddr_in *p2;

  memset ((void *)&hints, 0, sizeof (hints));
  hints.ai_family = AF_INET;
  status_net = getaddrinfo (param_target1_fqdn,
    param_dest_SAP,
    &hints,
    &results);
  if (status_net EQUALS 0)
  {
    p1 = results->ai_addr;
    p2 = (void *)p1;

    memcpy (&sa.sin_addr, (void *)&(p2->sin_addr), sizeof (p2->sin_addr));
  };
}

        err = connect(sd, (struct sockaddr *) &sa, sizeof(sa));
        if (err < 0) {
                fprintf(stderr, "Connect error\n");
                exit(1);
        }

        return sd;
}

/* closes the given socket descriptor.
 */
extern void tcp_close(int sd)
{
        shutdown(sd, SHUT_RDWR);        /* no more receptions */
        close(sd);
}

