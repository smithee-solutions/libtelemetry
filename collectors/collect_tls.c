/*
  collect_tls - collect TLS telemetry

  part of libtelemetry (github: smithee-us/libtelemetry)

  Usage:
    collect_tls <recorder-path> <dut-fqdn> <tls-parameters-gnutls>

  (C)Copyright 2017 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/


#include <stdio.h>
#include <time.h>
#include <string.h>

#include <gnutls/gnutls.h>


#define TLM_PARAM_DEFINE
#include <libtelemetry.h>


TLM_STREAM
  recorder_1;
TLM_STREAM
  recorder_2;
TLM_STREAM
  recorder_3;
TLM_STREAM
  recorder_4;
FILE
    *tlmlog;


int
  main
    (int
      argc,
    char
      *argv [])

{ /* main for collect_tls */

  char
    filename [1024];
  char
    recpath [1024];
  int
    status;
  TLM_CONTEXT
    tcontext;


  status = tlm_init (&tcontext);
  tlmlog = NULL;
  param_verbosity = tcontext.verbosity;
  if (param_verbosity > 0)
    fprintf (stderr, "%s\n", LIBTELEMETRY_VERSION_STRING);
  if (status != ST_OK)
    fprintf (stderr, "Init error.\n");
  param_verify = tcontext.verify_server;
  strcpy (param_target1_fqdn, "server-200.example.com");
  sprintf (filename, "/%s/results", TROOT);
  strcpy (param_results_path, filename);
  memset (param_priority, 0, sizeof (param_priority));
  strcpy (param_priority, "LEGACY:+VERS-SSL3.0");
  strcpy (param_dest_SAP, "443");
  if (argc > 1)
  {
    strcpy (param_results_path, argv [1]);
    if (argc > 2)
    {
      strcpy (param_target1_fqdn, argv [2]);
      if (argc > 3)
      {
        // if third arg is present it's the priority string
        // (unless it's "-" in which case use the default.)

        if (strcmp ("-", argv [3]) EQUALS 0)
        {
          strcpy (param_priority, "LEGACY:+VERS-SSL3.0");
        }
        else
        {
          memset (param_priority, 0, sizeof (param_priority));
          strcpy (param_priority, argv [3]);
        };
        if (argc > 4)
        {
          // if fourth arg is present it's the service access point
          // a/k/a tcp port number

          memset (param_dest_SAP, 0, sizeof (param_dest_SAP));
          strcpy (param_dest_SAP, argv [4]);
        };
      };
    };
  };
  strcpy (tcontext.server_fqdn, param_target1_fqdn);

  if (param_verbosity > 2)
  {
    fprintf (stderr, "telemetry: param_verbosity %d\n",
      param_verbosity);
  };
  /*
    init telemetry recorder that gets the TLS stuff
  */
  if (status EQUALS ST_OK)
    status = tlm_recorder_init (&recorder_1, TLM_STREAM_INIT, 1);
  if (status EQUALS ST_OK)
    status = tlm_recorder_set (&recorder_1, TLM_STREAM_FILENAME,
      (void *)param_results_path);
  if (status EQUALS ST_OK)
    status = tlm_recorder_set (&recorder_1, TLM_STREAM_FORMAT,
      (void *)TLM_FMT_TLS);
  if (status EQUALS ST_OK)
    status = tlm_recorder_set (&recorder_1, TLM_START_RECORDER,
      NULL);

  /*
    init telemetry recorder that captures character stream out of TLS connection
  */
  if (status EQUALS ST_OK)
    status = tlm_recorder_init (&recorder_2, TLM_STREAM_INIT, 2);
  if (status EQUALS ST_OK)
  {
    sprintf (recpath, "%s/%s", param_results_path,
      "recorder_02_output.log");
    status = tlm_recorder_set (&recorder_2, TLM_STREAM_FILENAME,
      (void *)recpath);
  };
  if (status EQUALS ST_OK)
    status = tlm_recorder_set (&recorder_2, TLM_START_RECORDER,
      NULL);
  if (status EQUALS ST_OK)
    status = tlm_recorder_init (&recorder_4, TLM_STREAM_INIT, 4);
  if (status EQUALS ST_OK)
  {
    sprintf (recpath, "%s/%s", param_results_path,
      "recorder_04_cert.der");

    status = tlm_recorder_set (&recorder_4, TLM_STREAM_FILENAME,
      (void *)recpath);
  };
  if (status EQUALS ST_OK)
    status = tlm_recorder_set (&recorder_4, TLM_START_RECORDER,
      NULL);

  /*
    init telemetry recorder that captures the log from this program.
    note we set a FILE variable (log) so the remainder of the code doesn't
    have to indulge the telemetry paradigm ;-)
  */
  if (status EQUALS ST_OK)
    status = tlm_recorder_init (&recorder_3, TLM_STREAM_INIT, 3);
  if (status EQUALS ST_OK)
  {
    sprintf (recpath, "%s/%s", param_results_path,
      "recorder_03.log");
    status = tlm_recorder_set (&recorder_3, TLM_STREAM_FILENAME,
      (void *)recpath);
  };
  if (status EQUALS ST_OK)
  {
    status = tlm_recorder_set (&recorder_3, TLM_START_RECORDER,
      NULL);
    tlmlog = recorder_3.file;

    fprintf (tlmlog, "collect_tls %s started.\n",
      LIBTELEMETRY_VERSION_STRING);
  };

  if (status EQUALS ST_OK)
  if (param_verbosity > 0)
  {
    fprintf (tlmlog, "Target FQDN: %s\n",
      param_target1_fqdn);
    fprintf (tlmlog, "Port: %s\n",
      param_dest_SAP);
    fprintf (tlmlog, "Server cert verification: ");
    if (param_verify EQUALS 0)
      fprintf (tlmlog, "DISABLED");
    else
      fprintf (tlmlog, "ENABLED");
    fprintf (tlmlog, "\n");
    fprintf (tlmlog, "Recorder 1: %s\n",
      recorder_1.recorder_path);
    fprintf (tlmlog, "Recorder 2: %s\n",
      recorder_2.recorder_path);
    fprintf (tlmlog, "Recorder 3: %s\n",
      recorder_3.recorder_path);
    fprintf (tlmlog, "Recorder 4: %s\n",
      recorder_4.recorder_path);

    // i.e. fflush tlmlog
    status = tlm_recorder_action (&recorder_3, TLM_STREAM_FLUSH);
  };

  if (status EQUALS ST_OK)
  {
    if (param_verbosity > 2)
      fprintf (stderr, "collect_tls: recording TLS telemetry...\n");
  };
  if (status EQUALS ST_OK)
  {
#define COLLECT_TLS_LABEL    ("TLM 1.1 (TLS)")
#define TLM_CIPHER_NAME       ("cipher_name")
#define TLM_ISSUER_COMMON_NAME ("issuer_common_name")
#define TLM_KEY_EXCHANGE_NAME ("key_exchange_name")
#define TLM_MAC_NAME          ("mac_name")
#define TLM_NOT_AFTER_TIME    ("not_after_time")
#define TLM_NOT_BEFORE_TIME   ("not_before_time")
#define TLM_SERIAL_HEX        ("serial_hex")
#define TLM_START_TIME_T      ("start_time")
#define TLM_SUBJ_COMMON_NAME  ("subj_common_name")
#define TLM_TLS_COMMENT       ("_comment")
#define TLM_TLS_VERSION       ("tls_version")
    fprintf (recorder_1.file,"{\n");
    fprintf (recorder_1.file,"\"%s\" : \"%s\",\n",
      TLM_TLS_COMMENT, COLLECT_TLS_LABEL);
    fprintf (recorder_1.file,"\"%s\" : \"%ld\",\n",
      TLM_START_TIME_T, time (NULL));
  };

  if (status EQUALS ST_OK)
  {
time_t t1;
char s1 [1024];
    status = tls_client (&tcontext);
    fprintf (recorder_1.file,"\"%s\" : \"%s\",\n",
      TLM_SUBJ_COMMON_NAME,
      recorder_1.tls_telemetry.subj_common_name);
strcpy (s1, recorder_1.tls_telemetry.serial_hex);
s1 [strlen (s1)-1] = 0;
    fprintf (recorder_1.file,"\"%s\" : \"%s\",\n",
      TLM_SERIAL_HEX, s1);
    fprintf (recorder_1.file,"\"%s\" : \"%s\",\n",
      TLM_ISSUER_COMMON_NAME,
      recorder_1.tls_telemetry.issuer_common_name);
    fprintf (recorder_1.file,"\"%s\" : \"%s\",\n",
      TLM_TLS_VERSION,
      recorder_1.tls_telemetry.tls_version);
    fprintf (recorder_1.file,"\"%s\" : \"%s\",\n",
      TLM_KEY_EXCHANGE_NAME,
      recorder_1.tls_telemetry.key_exchange_name);
    fprintf (recorder_1.file,"\"%s\" : \"%s\",\n",
      TLM_CIPHER_NAME,
      recorder_1.tls_telemetry.cipher_name);
    fprintf (recorder_1.file,"\"%s\" : \"%s\",\n",
      TLM_MAC_NAME,
      recorder_1.tls_telemetry.mac_name);
    fprintf (recorder_1.file,"\"%s\" : \"%ld\",\n",
      TLM_NOT_BEFORE_TIME,
      recorder_1.tls_telemetry.not_before_time);
    fprintf (recorder_1.file,"\"%s\" : \"%ld\",\n",
      TLM_NOT_AFTER_TIME,
      recorder_1.tls_telemetry.not_after_time);
t1 = recorder_1.tls_telemetry.not_before_time;
strcpy (s1, asctime (gmtime (&t1)));
s1 [strlen (s1)-1] = 0;
fprintf (recorder_1.file, "\"Not_Before_GMT\" : \"%s\",\n",
  s1);
t1 = recorder_1.tls_telemetry.not_after_time;
strcpy (s1, asctime (gmtime (&t1)));
s1 [strlen (s1)-1] = 0;
fprintf (recorder_1.file, "\"Not_After_GMT\" : \"%s\"\n", // NO COMMA LAST ONE
  s1);
    
    fprintf (recorder_1.file,"}\n");
  };

  if (status != ST_OK)
  {
    fprintf (stderr, "telemetry: collect_tls: return status off nominal: %d\n",
      status);
  };
  return (status);

} /* collect_tls */

