/*
  new-jacket - create a new jacket

  usage:
    new-jacket --fqdn <fqdn>
    new-jacket --ipv4 <ipv4>
    new-jacket --ipv6 <ipv6>
    new-jacket --ciderblock <cidr, ipv4 or ipv6>
      also --verbosity <n>
*/


#include <unistd.h>
#include <getopt.h>
#include <time.h>
#include <stdio.h>
#include <memory.h>


#include <gnutls/gnutls.h>


#include <libtelemetry.h>
int
  verbosity;
#define CRM_VERBOSITY (1)
#define CRM_FQDN      (2)


int
  main
    (int
      argc,
    char
      *argv [])

{ /* main for new-jacket */

  int
    done;
  char
    entry_name [1024];
  TLM_JACKET
    jacket;
  char
    jacket_format [1024];
  struct option
    longopts [] = {
      {"verbosity", required_argument, NULL, CRM_VERBOSITY},
      {"fqdn", required_argument, NULL, CRM_FQDN},
      {0, 0, 0, 0} };
  int
    option_index;
  char
    optstring [1024];
  int
    status;
  int
    status_options;


  status = ST_OK;
  verbosity = 1;
  memset (&jacket, 0, sizeof (jacket));
  strcpy (entry_name, "unknown");
  optstring [0] = 0;
  done =0;
  while (!done)
  {
    status_options = getopt_long (argc, argv, optstring, longopts,
     &option_index);
    if (status_options EQUALS -1)
      done = 1;
    if (!done)
    {
      if (verbosity > 8)
      {
        fprintf (stderr, "Parsing option %d\n", status_options);
      };
      switch (status_options)
      {
      default:
        fprintf (stderr, "getopt long opt index unknown (%d.)\n",
          option_index);
        break;
      case CRM_FQDN:
        strcpy (entry_name, optarg);
        strcpy (jacket_format, "new-fqdn");
        strcpy (jacket.fqdn, optarg);
        break;
      case CRM_VERBOSITY:
        {
          int i;
          sscanf (optarg, "%d", &i);
          verbosity = i;
        };
        break;
      };
    };
  };
  if (status EQUALS ST_OK)
  {
    status = create_jacket (&jacket, entry_name, jacket_format);
  };
  return (status);

} /* main for new-jacket */
