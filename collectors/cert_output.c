#include <stdio.h>
#include <time.h>
#include <string.h>


#include <gnutls/abstract.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <gnutls/compat.h>


#include <libtelemetry.h>
#include <tele_cert.h>


int
  add_extension_list_entry
    (TELE_CERT_CONTEXT
      *ctx,
    char
      *extension)

{ /* add_extension_list_entry */

  int
    i;
  int
    status;


  status = ST_TELE_CERT_UNK_EXTN;
  for (i=0; i<TELE_CERT_EXTN_MAX; i++)
  {
    if (0 EQUALS strcmp (ctx->extensions [i].name, extension))
    {
      sprintf (ctx->extensions [i].presence,
        "has-%s", ctx->extensions [i].name);
//      if (0==strcmp ("userPrincipleName", ctx->extensions [i].name))
        strcat (ctx->extensions [i].presence, ",1");
      status = ST_OK;
    }
    else
    {
    };
  };
  if (status EQUALS ST_TELE_CERT_UNK_EXTN)
  {
    if (ctx->verbosity > 3)
    {
      fprintf (stderr, "no entry for extension %s\n", extension);
    };
    ctx->extension_unknown ++;
    status = ST_OK;
  };
  return (status);

} /* add_extension_list_entry */


int
  init_tele_cert
    (TELE_CERT_CONTEXT
      *ctx)

{ /* init_tele_cert */

  int
    idx_ex;
  int
    status;

  status = ST_OK;
  memset (ctx, 0, sizeof (*ctx));
  ctx->verbosity = 9;

  // hand-initialize each extension list entry.
  // needs to be in fixed order so orderly spreadsheet is output in inventory mode.
  idx_ex = 0;
  strcpy (ctx->extensions [idx_ex].name, "1.3.6.1.5.5.7.1.11");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "2.16.840.1.101.3.6.9.1");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "authorityInfoAccess");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "authorityKeyIdentifier");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "basicConstraints");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "certificatePolicies");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "certsrvCAVersion");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "crlDistributionPoints");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "enrollCerttypeExtension");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "entrustVersInfo");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "extendedKeyUsage");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "hashedRootKey");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "id-ce-policyMappings");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "id-ce-subjectKeyIdentifier");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "id-pe-logotype");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "issuerAltName");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "keyUsage");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "nameConstraints");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "netscapeCertificateExtension");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "netscapeCertificateComment");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "privateKeyUsage");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "subjectAltName");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  strcpy (ctx->extensions [idx_ex].name, "userPrincipleName");
    sprintf (ctx->extensions [idx_ex].presence, "no-%s,0",
      ctx->extensions [idx_ex].name);
    idx_ex ++;
    if (idx_ex > (TELE_CERT_EXTN_MAX-1))
      fprintf (stderr, "Too many extensions for display config!\n");
  if (ctx->verbosity > 3)
    fprintf (stderr, "Initializing %d extension names\n", idx_ex);

  fprintf (stderr, "list-cert, part of libtelemetry %s\n",LIBTELEMETRY_VERSION_STRING);

  return (status);

} /* init_tele_cert */


int
  tele_cert_display_extensions
    (TELE_CERT_CONTEXT
      *ctx,
    gnutls_x509_crt_t
      cert)

{ /* tele_cert_display_extensions */

  unsigned int
    crit;
  char
    data [1024];
  size_t
    data_size;
  int
    done;
  int
    extension_index;
  int
    idx;
  char
    oid [1024];
  char
    oid_name_string [1024];
  size_t
    oid_size;
  int
    status;
  int
    status_gnutls;
        char eku_oid [1024];
        char eku_oid_name_string [1024];
        size_t eku_oid_length;
        char oid_tuple_s [1024];


  status = ST_OK;
  done = 0;
  extension_index = 0;
  while (!done)
  {
    oid_size = sizeof (oid);
    status_gnutls = gnutls_x509_crt_get_extension_info (cert, extension_index,
      oid, &oid_size, &crit);
    if (status_gnutls < 0)
      done = 1;
    else
    {
      oid_tuple_s [0] = '\0';
      data_size = sizeof (data);
      status_gnutls = gnutls_x509_crt_get_extension_data
        (cert, extension_index, data, &data_size);

      strcpy (oid_name_string, oid);
      if (0 EQUALS strcmp (oid, OID_NAME_AUTHORITYINFOACCESS))
      {
        strcpy (oid_name_string, OID_STRING_AUTHORITYINFOACCESS);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_AUTHORITY_KEY_IDENTIFIER))
      {
        strcpy (oid_name_string, OID_STRING_AUTHORITY_KEY_IDENTIFIER);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_BASICCONSTRAINTS))
      {
        strcpy (oid_name_string, OID_STRING_BASICCONSTRAINTS);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_CERTIFICATEPOLICIES))
      {
        strcpy (oid_name_string, OID_STRING_CERTIFICATEPOLICIES);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_CERTSRV_CA_VERSION))
      {
        strcpy (oid_name_string, OID_STRING_CERTSRV_CA_VERSION);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_CRLDISTRIBUTIONPOINTS))
      {
        strcpy (oid_name_string, OID_STRING_CRLDISTRIBUTIONPOINTS);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_ENTRUSTVERSION))
      {
        strcpy (oid_name_string, OID_STRING_ENTRUSTVERSION);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_CRLDISTRIBUTIONPOINTS))
      {
        strcpy (oid_name_string, OID_STRING_CRLDISTRIBUTIONPOINTS);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_ID_CE_SUBJECTKEYIDENTIFIER))
      {
        strcpy (oid_name_string, OID_STRING_ID_CE_SUBJECTKEYIDENTIFIER);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_ISSUERALTNAME))
      {
        strcpy (oid_name_string, OID_STRING_ISSUERALTNAME);
      };

      if (0 EQUALS strcmp (oid, OID_NAME_EXTENDED_KEY_USAGE))
      { 
          int done;
          int eku_idx;

        strcpy (oid_tuple_s, oid);
        strcpy (oid_name_string, OID_STRING_EXTENDED_KEY_USAGE);
        done = 0;
        eku_idx = 0;
        while (!done)
        {
          eku_oid_length = sizeof (eku_oid);
          status_gnutls = gnutls_x509_crt_get_key_purpose_oid (cert,
            eku_idx, eku_oid, &eku_oid_length, &crit);
          if (status_gnutls EQUALS 0)
          {
            strcpy (eku_oid_name_string, eku_oid);
            if (0 EQUALS strcmp (eku_oid, OID_NAME_ID_PIV_CONTENT_SIGNING))
              strcpy (eku_oid_name_string, OID_STRING_ID_PIV_CONTENT_SIGNING);
            if (ctx->output_format != OF_INVENTORY)
              printf ("    EKU %02d: %s\n", eku_idx, eku_oid_name_string);
            eku_idx ++;
          }
          else
            done = 1;
        };
      };
      if (0 EQUALS strcmp (oid, OID_NAME_HASHEDROOTKEY))
      {
        strcpy (oid_name_string, OID_STRING_HASHEDROOTKEY);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_KEYUSAGE))
      {
        strcpy (oid_name_string, OID_STRING_KEYUSAGE);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_LOGOTYPE))
      {
        strcpy (oid_name_string, OID_STRING_LOGOTYPE);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_MICROSOFT_CERT_TEMPLATE))
      { 
        strcpy (oid_tuple_s, oid);
        strcpy (oid_name_string, OID_STRING_MICROSOFT_CERT_TEMPLATE);
      };

      if (0 EQUALS strcmp (oid, OID_NAME_MICROSOFT_PRINCIPLE_NAME))
      { 
        strcpy (oid_name_string, OID_STRING_MICROSOFT_PRINCIPLE_NAME);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_MS_CERT_APP_POLICY))
      { 
        strcpy (oid_tuple_s, oid);
        strcpy (oid_name_string, OID_STRING_MS_CERT_APP_POLICY);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_MS_ENROLL_CERTTYPE_EXTN))
      { 
        strcpy (oid_tuple_s, oid);
        strcpy (oid_name_string, OID_STRING_MS_ENROLL_CERTTYPE_EXTN);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_NAMECONSTRAINTS))
      {
        strcpy (oid_name_string, OID_STRING_NAMECONSTRAINTS);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_NETSCAPECERTEXTN))
      {
        strcpy (oid_name_string, OID_STRING_NETSCAPECERTEXTN);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_NETSCAPECERTCOMMENT))
      {
        strcpy (oid_name_string, OID_STRING_NETSCAPECERTCOMMENT);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_POLICYMAPPINGS))
      {
        strcpy (oid_name_string, OID_STRING_POLICYMAPPINGS);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_PRIVATEKEYUSAGE))
      {
        strcpy (oid_name_string, OID_STRING_PRIVATEKEYUSAGE);
      };
      if (0 EQUALS strcmp (oid, OID_NAME_SUBJECTALTNAME))
      {
        strcpy (oid_name_string, OID_STRING_SUBJECTALTNAME);
      };

        if (ctx->output_format EQUALS OF_INVENTORY)
        {
          status = add_extension_list_entry (ctx, oid_name_string);
        }
        else
        {
          if (*oid_tuple_s EQUALS '\0')
            printf ("  Extension %03d %d: %s\n",
              extension_index, status_gnutls, oid_name_string);
          else
            printf ("  Extension %03d %d: %s (%s)\n",
              extension_index, status_gnutls, oid_name_string,
              oid_tuple_s);
          for (idx=0; idx<data_size; idx++)
          {
            printf (" %02x", *((unsigned char *)data + idx));
            if (15 EQUALS (idx % 16))
              printf ("\n");
          }
          if (15 != (data_size % 16))
            printf ("\n");
        };
        extension_index ++;
      };
    };

  return (status);

} /* tele_cert_display_extensions */


char
  *tele_cert_extension_inventory
    (TELE_CERT_CONTEXT
      *ctx)

{ /* tele_cert_extension_inventory */

  int
    i;
  static char
    inventory [TELE_CERT_EXTN_MAX*1024];

  inventory [0] = 0;
  for (i=0; i<TELE_CERT_EXTN_MAX; i++)
  {
    // if there's something there...
    if (strlen (ctx->extensions [i].name) > 0)
    {
      strcat (inventory, ",");
      strcat (inventory, ctx->extensions [i].presence);
    };
  };
  return (inventory); // my static, use quickly!

} /* tele_cert_extension_inventory */


int
  tele_display_subject_alt_name
    (TELE_CERT_CONTEXT
      *ctx,
    gnutls_x509_crt_t
      cert,
    int
      *san_entry_count,
    char
      *san_inventory,
    int
      *san_inventory_length)

{ /* tele_display_subject_alt_name */

  unsigned int
    crit;
  char
    *crit_tag;
  int
    done;
  char
    other_name [1024];
  char
    other_oid [1024];
  size_t
    other_oid_size;
  char
    san [64*1024];
  int
    san_idx;
  size_t
    san_size;
  char
    san_text [64*1024];
  int
    status_gnutls;
  int
    status;
  char tmp_s [1024];


  status = ST_OK;
  /*
    extract all the subjectAltName fields and display as
    far as possible.
  */
  san_inventory [0] = 0;
  done = 0;
  san_idx = 0;
  while (!done)
  {
    san_size = sizeof (san);
    crit = 0;
    status_gnutls = gnutls_x509_crt_get_subject_alt_name
      (cert, san_idx, san, &san_size, &crit);
    if (crit)
      crit_tag = "(Critical)";
    else
      crit_tag = "";
    switch (status_gnutls)
    {
    default:
      fprintf (stderr, "Unknown subjectAltName get-status (%d)\n",
        status_gnutls);
      done = 1;
      break;
    case GNUTLS_SAN_DNSNAME:
      sprintf (san_text,
        "subjectAltname[%02d].dnsName:%s%s ",
          san_idx, san, crit_tag);
      if (ctx->output_format EQUALS OF_INVENTORY)
      {
        if ((strlen (san_inventory) + strlen (san_text)) >
            *san_inventory_length)
          fprintf (stderr, "subjectAltName text overflow: %s\n",
            san_inventory);
        else
          strcat (san_inventory, san_text);
      }
      else
      {
        printf ("\tsubjectAltname[%02d].dnsName: %s (Criticality: %d)\n",
          san_idx, san, crit);
      };
      san_idx++;
      break;
    case GNUTLS_SAN_IPADDRESS:
{
  int i1;
  i1=0xff & san[0];
      sprintf(tmp_s, "%u.%u.%u.%u",
        i1, san [1], san [2], 0xff & san[3]);
};
      sprintf (san_text,
        "subjectAltname[%02d].ipaddress:%s%s\n",
          san_idx, tmp_s, crit_tag);
      if (ctx->output_format EQUALS OF_INVENTORY)
      {
        if ((strlen (san_inventory) + strlen (san_text)) >
            *san_inventory_length)
          fprintf (stderr, "subjectAltName text overflow: %s\n",
            san_inventory);
        else
          strcat (san_inventory, san_text);
      }
      else
        printf ("\t%s\n", san_text);
      san_idx++;
      break;
    case GNUTLS_SAN_OTHERNAME:
      other_oid_size = sizeof (other_oid);
      status_gnutls = gnutls_x509_crt_get_subject_alt_othername_oid
        (cert, san_idx, &other_oid, &other_oid_size);
      if (status_gnutls EQUALS GNUTLS_SAN_OTHERNAME)
      {
        int idx;

        strcpy (other_name, other_oid);

        if (0 EQUALS strcmp (other_oid, OID_NAME_MICROSOFT_PRINCIPLE_NAME))
          strcpy (other_name, OID_STRING_MICROSOFT_PRINCIPLE_NAME);
        if (0 EQUALS strcmp (other_oid, OID_NAME_PIV_FASC_N))
          strcpy (other_name, OID_STRING_PIV_FASC_N);
        if (ctx->output_format != OF_INVENTORY)
        {
          printf ("\tsubjectAltname[%02d].otherName: %s (Criticality: %d), Data:\n",
            san_idx, other_name, crit);
          for (idx=0; idx<san_size; idx++)
            printf (" %02x", (unsigned char)*(san+idx));
          printf ("\n");
        };
      };
      san_idx++;
      break;
    case GNUTLS_SAN_RFC822NAME:
      sprintf (san_text,
        "subjectAltname[%02d].rfc822Name:%s%s ",
          san_idx, san, crit_tag);
      if (ctx->output_format EQUALS OF_INVENTORY)
      {
        if ((strlen (san_inventory) + strlen (san_text)) >
            *san_inventory_length)
          fprintf (stderr, "subjectAltName text overflow: %s\n",
            san_inventory);
        else
          strcat (san_inventory, san_text);
      }
      else
      {
        printf ("\tsubjectAltname[%02d].rfc822Name: %s (Criticality: %d)\n",
          san_idx, san, crit);
      };
      san_idx++;
      break;
    case GNUTLS_E_REQUESTED_DATA_NOT_AVAILABLE:
      done = 1;
      break;
    };
  };
//	GNUTLS_SAN_URI = 3, GNUTLS_SAN_IPADDRESS = 4, GNUTLS_SAN_OTHERNAME = 5, GNUTLS_SAN_DN = 6, GNUTLS_SAN_OTHERNAME_XMPP = 1000

  *san_inventory_length = strlen (san_inventory);
  *san_entry_count = san_idx;

  return (status);

} /* tele_display_subject_alt_name */


int
  tele_jacket_append
  (TELE_CERT_CONTEXT
      *ctx,
  char
    *parameter_name,
  char
    *parameter_value)

{ /* tele_jacket_append */

  char
    jacket_filename [1024];
  FILE
    *jf;
  int
    status;


  status = ST_OK;
  sprintf (jacket_filename, "%s.jacket", ctx->cert_file_base);
  jf = fopen (jacket_filename, "a+");
  fprintf (jf,
    "\"%s\" : \"%s\"\n",
    parameter_name, parameter_value);
  fclose (jf);
  return (status);

} /* tele_jacket_append */

