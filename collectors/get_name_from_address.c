#include <stdio.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>

int main
  (
    int
      argc,
    char
      *argv [])

{
  char
    addr_to_find [1024];
  struct sockaddr_in sa;
  sa.sin_family = AF_INET;

  char node [NI_MAXHOST];

  strcpy (addr_to_find, "10.2.0.2");
  if (argc > 1)
    strcpy (addr_to_find, argv [1]);
//  fprintf (stderr, "Addr to find: %s\n", addr_to_find);
  inet_pton (AF_INET, addr_to_find, &sa.sin_addr);
  int res = getnameinfo ((struct sockaddr *)&sa, sizeof (sa), node,
    sizeof (node), NULL, 0, NI_NAMEREQD);
  if (res)
  {
    fprintf (stderr, "could not find name info: %s - %s\n", addr_to_find, gai_strerror (res));
    node [0] = 0;
  }
  printf ("%s\n", node);
  return (0);

}

