#include <string.h>


int param_verbosity=9;
/*
  ana_tls - analyze TLS telemetry

  libtelemetry - network telemetry tools and support

  (C)Copyright 2017 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/


#include <stdio.h>

#include <gnutls/gnutls.h>


#define TLM_PARAM_DEFINE
#include <libtelemetry.h>


int
  main
    (int
      argc,
    char
      *argv [])

{ /* ana_tls */

  TLM_STREAM
    recorder_1;
  int
    status;
  TLM_CONTEXT tcontext;


  status = ST_OK;
  memset(&tcontext, 0, sizeof(tcontext));
  tcontext.verbosity = 999;
  if (tcontext.verbosity > 1)
    fprintf (stderr, "telemetry: param_verbosity %d\n",
      tcontext.verbosity);
  if (status EQUALS ST_OK)
    status = tlm_recorder_init (&tcontext, &recorder_1, TLM_STREAM_INIT, 1);
  if (status EQUALS ST_OK)
  {
    status = tlm_recorder_set (&recorder_1, TLM_RECORDER_PLAY, NULL);
  };
// read
  if (status != ST_OK)
  {
    fprintf (stderr, "telemetry: collect_tls: return status off nominal: %d\n",
      status);
  };
  return (status);

} /* ana_tls */

