/*
  groom-nmap-xml - given an nmap xml output file create a telemetry json snippet
*/


#include <stdio.h>
#include <string.h>
#include <expat.h>

#include <gnutls/gnutls.h>

#include <libtelemetry.h>

typedef struct nmap_context
{
  int
    state;
} NMAP_CONTEXT;

// arbitrarily, so this thing doesn't try to grow up
// to be a scanner, cap this at a fixed 256 element array.
// code can still scale but this version has limits.

typedef struct ports_open
{
  char
    sap_port [6]; // 99999<nul>
} PORTS_OPEN;
typedef struct node_inventory
{
  char
    addr_ipv4 [16]; //999.999.999.999<nul>
  char
    addr_ieee802 [2*6 + 5 + 1]; // aa:bb:cc:dd:ee:ff<nul>
  int
    port_count;
  PORTS_OPEN
    ports[8];
} NODE_INVENTORY;
#define MAX_NODES (254)

#define STATE_START (0)
#define STATE_FOUND_HOST (1)
#define STATE_HOST_ADDRESS (2)
#define STATE_HOST_PORT    (3)
#define EQUALS ==


void start_element
  (void
    *data,
  const char
    *element,
  const char
    **attribute);

int
  depth;
char
  *last_content;
NMAP_CONTEXT
  nctx;
int
  cur_netnode;
int
  cur_sap;
NODE_INVENTORY
  netnodes [MAX_NODES];
char
  param_results_path [1024];
int
  param_verbosity;

char save1 [1024];
char save2 [1024];
char save3 [1024];
char save4 [1024];


void
  check_port
    (char
       *port_number)

{ /* check_port */

  if (strcmp ("21", port_number) EQUALS 0)
    printf ("Warning: (Unencrypted) File Transfer (FTP, TCP/21) service in use.  Confirm traffic disclosure is acceptable.\n");
  if (strcmp ("53", port_number) EQUALS 0)
    printf ("Warning: Domain Name (DNS, TCP/53) service in use.  Confirm network service complies with local policy.\n");
  if (strcmp ("80", port_number) EQUALS 0)
    printf ("Warning: Unencrypted browser (HTTP, TCP/80) service in use.  Confirm traffic disclosure is acceptable.\n");
  if (strcmp ("3389", port_number) EQUALS 0)
    printf ("Warning: Windows Remote Desktop (RDP, TCP/3389) service in use.  Confirm remote Windows access complies with local policy.\n");

} /* check_port */


void
  end_element
    (void
      *data,
    const char
      *el)

{ /* end_element */

  if (param_verbosity > 3)
    fprintf (stderr, "d %d end host %d address state,collect details...\n",
      depth, cur_netnode);
  switch (nctx.state)
  {
  default:
    if (param_verbosity > 3)
      fprintf (stderr, "end, state %d element %s\n", nctx.state,
        el);
    break;
  case STATE_FOUND_HOST:
    if (param_verbosity > 3)
      fprintf (stderr, "end, found host, depth %d\n",
        depth);
    if (depth EQUALS 2)
    {
      nctx.state = STATE_START;
    };
    break;
  case STATE_HOST_ADDRESS:
    nctx.state = STATE_FOUND_HOST;
    if (param_verbosity > 3)
      fprintf (stderr, "d %d end host %d address state,collect details...\n",
        depth, cur_netnode);
    if (strcmp (save4, "ipv4") EQUALS 0)
    {
      if (strcmp (save1, "addr") EQUALS 0)
      {
        strcpy (netnodes [cur_netnode].addr_ipv4, save2);
      };
    };
    if (strcmp (save4, "mac") EQUALS 0)
    {
      if (strcmp (save1, "addr") EQUALS 0)
      {
        strcpy (netnodes [cur_netnode].addr_ieee802, save2);
      };
    };
    break;
  case STATE_HOST_PORT:
    nctx.state = STATE_FOUND_HOST;
    if (strcmp (save2, "tcp") EQUALS 0)
    {
      if (strcmp (save3, "portid") EQUALS 0)
      {
        strcpy (netnodes [cur_netnode].ports [cur_sap].sap_port, save4);
        netnodes [cur_netnode].port_count = 1+cur_sap;
      };
    };
    break;
  };
  depth--;

} /* end_element */


void
  handle_data
    (void
      *data,
    const char
      *content,
    int
      length)
{
  char *tmp = malloc (length);

  strncpy (tmp, content, length);
  tmp [length] = 0;
  data = (void *)tmp;
  last_content = tmp;

} /* handle_data */


int
  main
    (int argc,
    char *argv [])

{ /* main for groom-nmap-xml */

  char buff [65536];
  int buff_size;
  int done;
  char filename [1024];
  FILE *fp;
  int i;
  int j;
  XML_Parser parser;
  int status;
  int status_io;


  status = ST_OK;
  buff_size = sizeof (buff);
if (argc > 1)
{
  fprintf (stderr, "Groom NMAP XML 0.0 EP 01\n");
};

  param_verbosity = 2;
  sprintf (filename, "/%s/results", TROOT);
  strcpy (param_results_path, filename);
  if (argc > 1)
  {
    strcpy (param_results_path, argv [1]);
    if (argc > 2)
    {
      int i;
      sscanf (argv [1], "%d", &i);
      param_verbosity = i;
    };
  };

  if (param_verbosity > 2)
  {
    fprintf (stderr, "collect_nmap: param_verbosity %d\n",
      param_verbosity);
  };

  cur_netnode = -1;
  cur_sap = -1;
  memset (netnodes, 0, sizeof (netnodes));
  fp = fopen ("test.in", "r");
  if (fp == NULL)
  {
    printf ("Failed to open file\n");
    status = -1;
  }

  if (status EQUALS ST_OK)
  {
    parser = XML_ParserCreate (NULL);
    XML_SetElementHandler(parser, start_element, end_element);
    XML_SetCharacterDataHandler (parser, handle_data);

    memset(buff, 0, buff_size);
    memset((void *)&nctx, 0, sizeof (nctx));

    status_io = fread (buff, sizeof(char), buff_size, fp);
    if (param_verbosity > 1)
      fprintf (stderr, "Read %d bytes from file\n", status_io);
    if (status_io EQUALS buff_size)
      status = -2;

    if (status EQUALS ST_OK)
    {
      /* parse the xml */
      if (XML_Parse(parser, buff, strlen(buff), XML_TRUE) == XML_STATUS_ERROR)
      {
        printf("Error: %s\n", XML_ErrorString(XML_GetErrorCode(parser)));
      }
    };
    XML_ParserFree (parser);
  };

  fprintf (stderr, "%d nodes found\n",
    1+cur_netnode);
  done = 0;
  for (i=0; (!done) && (i<cur_netnode); i++)
  {
    char filename [1024]; 
    FILE *ninfo;
    char s1 [1024];


    sprintf (filename, "%s/%06d_node_inventory.json",
      param_results_path, i);
    ninfo = fopen (filename, "w");
    if (!ninfo)
      done = 1;
    if (!done)
    {
      fprintf (ninfo, "{\n");
      if (strlen (netnodes[i].addr_ieee802) EQUALS 0)
        strcpy (s1, "(Unknown)");
      else
        strcpy (s1, netnodes[i].addr_ieee802);

      fprintf (ninfo, "  \"ipv4\" : \"%s\",\n  \"mac\" : \"%s\",\n",
        netnodes [i].addr_ipv4, s1);
      for (j=0; j < netnodes [i].port_count; j++)
      {
        fprintf (ninfo, "  \"port\" : \"tcp/%s\"",
          netnodes [i].ports [j].sap_port);
        if (j != (netnodes[i].port_count-1))
          fprintf (ninfo, ",");
        fprintf (ninfo, "\n");
      };
      fprintf (ninfo, "}\n");
      fclose (ninfo);
    };
  };

  if (status != 0)
    fprintf (stderr, "Status was %d\n", status);
  return (status);

} /* main for groom-nmap-xml */


void start_element
  (void
    *data,
  const char
    *element,
  const char
    **attribute)

{ /* start_element */

  if (param_verbosity > 3)
    fprintf (stderr, "New element depth %d state %d element %s\n",
      depth, nctx.state, element);
  switch (nctx.state)
  {
  default:
    if (param_verbosity > 3)
      fprintf (stderr, "(default %d) start element: %s\n",
        nctx.state, element);
    if (strcmp (element, "host") EQUALS 0)
    {
      nctx.state = STATE_FOUND_HOST;
      cur_netnode ++;
      cur_sap = -1;
    };
{
  char *p;
  char **pp;
  int done;
  pp = (char **)attribute;
  done = 0;
  if (!pp)
    done=1;
  while (!done)
  {
    p = *pp;
    if (!p)
      done=1;
    if (!done)
    {
      if (param_verbosity > 3)
        fprintf (stderr, "dlist-%d element %s attribute %s\n",
          nctx.state, element, p);
      pp++;
      if (!pp)
        done=1;
    };
  };
};
    break;
  case STATE_FOUND_HOST:
    if (strcmp (element, "address") EQUALS 0)
      nctx.state = STATE_HOST_ADDRESS;
    if (strcmp (element, "port") EQUALS 0)
    {
      nctx.state = STATE_HOST_PORT;
      cur_sap ++;
    };
    if (param_verbosity > 3)
      fprintf (stderr, "(host %d) start element: %s\n",
        nctx.state, element);
{
  char *p;
  char **pp;
  int done;
  pp = (char **)attribute;
  done = 0;
  if (!pp)
    done=1;
  while (!done)
  {
    p = *pp;
    if (!p)
      done=1;
    if (!done)
    {
      if (param_verbosity > 3)
        fprintf (stderr, "list-%d element %s attribute %s port idx %d\n",
          nctx.state, element, p, cur_sap);

      // if we're processing an address element look for thing1/thing2 pairs
      switch (nctx.state)
      {
      default:
        break;
      case STATE_HOST_ADDRESS:
        if (strcmp (p, "addr") EQUALS 0)
        {
          strcpy (save1, p);
          strcpy (save2, (char *)*(pp+1));
          strcpy (save3, (char *)*(pp+2));
          strcpy (save4, (char *)*(pp+3));
        };
        break;
      case STATE_HOST_PORT:
        if (strcmp (p, "protocol") EQUALS 0)
        {
          strcpy (save1, p);
          strcpy (save2, (char *)*(pp+1));
          strcpy (save3, (char *)*(pp+2));
          strcpy (save4, (char *)*(pp+3));
        };
        break;
      };

      pp++;
      if (!pp)
        done=1;
    };
  };
};
    break;
  };
  depth++;

} /* start_element */

