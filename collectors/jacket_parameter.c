/*
  jacket_parameter - extract a parameter to stdout

  usage:

    jacket_parameter jacket_filename parameter_string
*/

#include <stdio.h>
#include <string.h>


#include <jansson.h>
#include <gnutls/gnutls.h>


#include <libtelemetry.h>

int verbosity = 1;


int
  main
    (int
      argc,
    char
      *argv [])

{ /* main for jacket_parameter */

  TLM_JACKET
    jacket;
  char
    parameter [1024];
  int
    status;


  parameter [0] = 0;
  if (argc > 1)
    strcpy (parameter, argv [2]);
  status = read_jacket (argv [1], &jacket);
  if (status EQUALS 0)
  {
    if (0 == strcmp ("dsap_0", parameter))
    {
      fprintf (stdout, "%s", jacket.dsap_0);
      fflush (stdout);
    };
    if (0 == strcmp ("dsap_1", parameter)) { fprintf (stdout, "%s", jacket.dsap_1); };
      fflush (stdout);
    if (0 == strcmp ("dsap_2", parameter)) { fprintf (stdout, "%s", jacket.dsap_2); };
      fflush (stdout);
    if (0 == strcmp ("fqdn", parameter)) { fprintf (stdout, "%s", jacket.fqdn); };
      fflush (stdout);
    if (0 == strcmp ("ipv4", parameter))
    {
      fprintf (stdout, "%s", jacket.ipv4);
    };
    fflush (stdout);
    if (0 == strcmp ("mac", parameter))
    {
      fprintf (stdout, "%s", jacket.mac);
    };
    if (0 == strcmp ("model", parameter))
    {
      fprintf (stdout, "%s", jacket.model);
    };
    fflush (stdout);
    if (0 == strcmp ("software_version", parameter)) { fprintf (stdout, "%s", jacket.software_version); };
      fflush (stdout);
    if (0 == strcmp ("vendor", parameter))
    {
      fprintf (stdout, "%s", jacket.vendor);
    };
  };
  return (status);

} /* main for jacket_parameter */

