#ifndef LIBTELEMETRY_H
#define LIBTELEMETRY_H
/*
  libtelemetry.h - definitions for libtelemetry

  (C)Copyright 2017-2018 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

#define LIBTELEMETRY_VERSION_STRING "2.11"
#define TROOT "opt/tester"


typedef struct tlm_context
{
  char
    server_fqdn[1024];
  int
    verbosity;
  char
    results_path [1024];

  // admittedly disjoint parameters but parsed from telemetry json config file
  int
    verify_server; // 1 for enabled
} TLM_CONTEXT;


typedef struct tlm_tls_client_connect
{
  char tls_version [1024];
  char key_exchange_name [1024];
  char cipher_name [1024];
  char mac_name [1024];
  char cipher_suite_name [1024];
  char subj_common_name [1024];
  char issuer_common_name [1024];
  time_t not_before_time;
  time_t not_after_time;
  char serial_hex [1024];
  int critical;
  char certificate_authority [1024];
  int path_length;
} TLM_TLS_CLIENT_CONNECT;

typedef struct tlm_stream
{
  int
    stream_id;
  int
    stream_format;
  FILE
    *file;
  char
    recorder_path [1024];
  TLM_TLS_CLIENT_CONNECT
    tls_telemetry;
} TLM_STREAM;

typedef struct tlm_jacket
{
  char
    tag [1024];
  char
    comment [1024];
  char
    version [1024];
  char
    jacket_directory [1024];
  char
    fqdn [1024];
  char
    ipv4 [1024];
  char
    name [1024];
  int
    proto_list_size;
  char
    proto_list [8*64];
  char
    ranking [1024];
  char
    mac [1024];
  char
    software_version [1024];
  char
    vendor [1024];
  char
    model [1024];
  char
    dsap_0 [1024];
  char
    dsap_1 [1024];
  char
    dsap_2 [1024];
  char
    dsap_3 [1024];
  char
    dsap_4 [1024];
} TLM_JACKET;

// for ICMP and future low level packet stuff

typedef struct tlm_pdu_context
{
  int
    verbosity;
  char
    destination [1024];
  char
    source [1024];
  int
    results_length;
  unsigned char
    results [1024];
  unsigned short int
    parameter_1;
  unsigned short int
    indicator_1;
  unsigned short int
    indicator_2;
} TLM_PDU_CONTEXT;


#define TLM_RM_TAG_FQDN "fqdn"
#define TLM_RM_TAG_SUBJECT_DN "subject"


#define TLM_STREAM_INIT   (1)
#define TLM_STREAM_REPLAY (2)
#define TLM_STREAM_ENGAGE (3)
#define TLM_START_RECORDER (4)
#define TLM_RECORDER_PLAY  (5)

// commands to set operation
#define TLM_STREAM_NOOP     (0)
#define TLM_STREAM_FORMAT   (1)
#define TLM_STREAM_FILENAME (2)
#define TLM_STREAM_FLUSH    (3)
#define TLM_STREAM_CLOSE    (4)

// formats
#define TLM_FMT_TEXTLOG   (1)
#define TLM_FMT_TLS       (2)
#define TLM_FMT_CHAR_DATA (3)

// defaults
#define TLM_DEFAULT_VERBOSITY (2)

#define EQUALS ==
#define UINT32 unsigned long int

#define ST_OK                     ( 0)
#define ST_STUB         (1)
#define ST_STREAM_ERROR (2)
#define ST_BAD_FQDN     (3)
#define ST_BAD_REC2     (4)
#define ST_BAD_REC_ENGAGE (5)
#define ST_TLM_SET_UNKNOWN (6)
#define ST_TLM_REC_BAD_PATH (7)
#define ST_TLM_BAD_RECORDER       ( 8)
#define ST_JACKET_OVERFLOW        ( 9)
#define ST_JACKET_UNDERFLOW       (10)
#define ST_BAD_JACKET             (11)
#define ST_JACKET_PARSE_IPV4      (12)
#define ST_JACKET_PARSE           (13)
#define ST_JACKET_PARSE_MAC       (14)
#define ST_JACKET_PARSE_VENDOR    (15)
#define ST_JACKET_PARSE_FQDN      (16)
#define ST_JACKET_PARSE_PROTOCOLS (17)
#define ST_JACKET_PARAM_UNKNOWN   (18)
#define ST_CMD_UNDERFLOW             (19)
#define ST_CMD_OVERFLOW              (20)
#define ST_CMD_ERROR                 (21)
#define ST_ADDR_ERROR                (22)
#define ST_BAD_NMAP_FILE             (23)
#define ST_PARSE_NMAP_NO_NMAPRUN     (24)
#define ST_PARSE_NMAP_NO_HOST        (25)
#define ST_PARSE_NMAP_HOST_ADDRESSES (26)
#define ST_PARSE_NMAP_ATNAME         (27)
#define ST_PARSE_NMAP_HOSTNAME       (28)
#define ST_JACKET_PARSE_DSAP0        (29)
#define ST_JACKET_PARSE_DSAP1        (30)
#define ST_JACKET_PARSE_DSAP2        (31)
#define ST_JACKET_WRITE              (32)
#define ST_JACKET_FMT_UNK            (33)
#define ST_JACKET_FMT_UNIMP          (34)
#define ST_JACKET_VENDOR_ERROR       (35)
#define ST_JACKET_PARSE_SWVERS       (36)
#define ST_JACKET_PARSE_TAG          (37)
#define ST_JACKET_PARSE_COMMENT      (38)
#define ST_JACKET_PARSE_VERSION      (40)
#define ST_JACKET_PARSE_NAME         (41)
#define ST_JACKET_PARSE_RANKING      (42)
#define ST_TLM_CFG_READ              (43)
#define ST_TLM_CFG_UNDERFLOW         (44)
#define ST_TLM_CFG_OVERFLOW          (45)
#define ST_TLM_CFG_ERROR             (46)
#define ST_TLM_PING4_TIMEOUT         (47)
#define ST_TLM_PING4_NET_1           (48)
#define ST_TLM_PING4_NET_2           (49)
#define ST_TLM_PING4_NET_3           (50)
#define ST_TLM_PING4_NET_4           (51)
#define ST_TLM_NEEDS_ROOT            (52)

int
  tlm_recorder_init
    (TLM_CONTEXT *tctx,
    TLM_STREAM *recorder,
    int record_command,
    int new_stream_id);
int
  tlm_recorder_set
    (TLM_STREAM
      *recorder,
    int
      tlm_set_cmd,
    void
      *tlm_set_value1);
int
  tlm_recorder_action (TLM_STREAM *recorder, int cmd);

#ifdef TLM_PARAM_DEFINE
char param_dest_SAP [1024];
char param_priority [1024];
char param_target1_fqdn [1024];
int param_verify;
#endif
#ifndef TLM_PARAM_DEFINE
extern char param_dest_SAP []; // Service Access Point (tcp port)
extern char param_priority [];
extern char param_target1_fqdn [1024];
extern int param_verify;
#endif

// for gnutls integration...
void
  check_alert(gnutls_session_t session, int ret);

int
  write_pkcs12(const gnutls_datum_t * cert,
                 const gnutls_datum_t * pkcs8_key, const char *password);

void
  verify_certificate(gnutls_session_t session, const char *hostname);

int
  print_info(gnutls_session_t session);

void print_x509_certificate_info(TLM_CONTEXT *tctx, gnutls_session_t session);

int
  _ssh_verify_certificate_callback(gnutls_session_t session);

void
  verify_certificate_chain(const char *hostname,
                         const gnutls_datum_t * cert_chain,
                         int cert_chain_length);

int
  verify_certificate_callback(gnutls_session_t session);

// for jacket management

int
  read_jacket
    (char
      *jacket_name,
    TLM_JACKET
      *jacket);
int
  tlm_init
    (TLM_CONTEXT
      *ctx);
int
  tls_client
    (TLM_CONTEXT
      *tctx);
int
  update_jacket
    (TLM_JACKET
      *jacket,
    char
      *parameter,
    char
      *value);
int
  validate_jacket_entry_name
    (char * jacket_format,
    char * entry_name);

// for ICMP telemetry

int
  ping_ipv4
    (TLM_PDU_CONTEXT
      *ctx);

#endif /* LIBTELEMETRY_H */

