/*
  tele-cert.h - definitions for libtelemetry -> list-cert

  (C)Copyright 2017-2018 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

// OID definitions

#define OID_NAME_ENTRUSTVERSION               "1.2.840.113533.7.65.0"
#define OID_STRING_ENTRUSTVERSION             "entrustVersInfo"
#define OID_NAME_MICROSOFT_PRINCIPLE_NAME     "1.3.6.1.4.1.311.20.2"
#define OID_STRING_MICROSOFT_PRINCIPLE_NAME   "userPrincipleName"
#define OID_NAME_MS_ENROLL_CERTTYPE_EXTN      "1.3.6.1.4.1.311.20.2.3"
#define OID_STRING_MS_ENROLL_CERTTYPE_EXTN    "enrollCerttypeExtension"
  // szOID_NT_PRINCIPLE_NAME in vendor doc
#define OID_NAME_CERTSRV_CA_VERSION           "1.3.6.1.4.1.311.21.1"
#define OID_STRING_CERTSRV_CA_VERSION         "certsrvCAVersion"
#define OID_NAME_MICROSOFT_CERT_TEMPLATE      "1.3.6.1.4.1.311.21.7"
#define OID_STRING_MICROSOFT_CERT_TEMPLATE    "certificateTemplate"
#define OID_NAME_MS_CERT_APP_POLICY           "1.3.6.1.4.1.311.21.10"
#define OID_STRING_MS_CERT_APP_POLICY         "certificateApplicationPolicy"
#define OID_NAME_AUTHORITYINFOACCESS          "1.3.6.1.5.5.7.1.1"
#define OID_STRING_AUTHORITYINFOACCESS        "authorityInfoAccess"
#define OID_NAME_LOGOTYPE                     "1.3.6.1.5.5.7.1.12"
#define OID_STRING_LOGOTYPE                   "id-pe-logotype"
#define OID_NAME_HASHEDROOTKEY                "2.23.42.7.0"
#define OID_STRING_HASHEDROOTKEY              "hashedRootKey"
#define OID_NAME_ID_CE_SUBJECTKEYIDENTIFIER   "2.5.29.14"
#define OID_STRING_ID_CE_SUBJECTKEYIDENTIFIER "id-ce-subjectKeyIdentifier"
#define OID_NAME_KEYUSAGE                     "2.5.29.15"
#define OID_STRING_KEYUSAGE                   "keyUsage"
#define OID_NAME_PRIVATEKEYUSAGE              "2.5.29.16"
#define OID_STRING_PRIVATEKEYUSAGE            "privateKeyUsage"
#define OID_NAME_SUBJECTALTNAME               "2.5.29.17"
#define OID_STRING_SUBJECTALTNAME             "subjectAltName"
#define OID_NAME_ISSUERALTNAME                "2.5.29.18"
#define OID_STRING_ISSUERALTNAME              "issuerAltName"
#define OID_NAME_BASICCONSTRAINTS             "2.5.29.19"
#define OID_STRING_BASICCONSTRAINTS           "basicConstraints"
#define OID_NAME_NAMECONSTRAINTS              "2.5.29.30"
#define OID_STRING_NAMECONSTRAINTS            "nameConstraints"
#define OID_NAME_CRLDISTRIBUTIONPOINTS        "2.5.29.31"
#define OID_STRING_CRLDISTRIBUTIONPOINTS      "crlDistributionPoints"
#define OID_NAME_CERTIFICATEPOLICIES          "2.5.29.32"
#define OID_STRING_CERTIFICATEPOLICIES        "certificatePolicies"
#define OID_NAME_POLICYMAPPINGS               "2.5.29.33"
#define OID_STRING_POLICYMAPPINGS             "id-ce-policyMappings"
#define OID_NAME_AUTHORITY_KEY_IDENTIFIER     "2.5.29.35"
#define OID_STRING_AUTHORITY_KEY_IDENTIFIER   "authorityKeyIdentifier"
#define OID_NAME_EXTENDED_KEY_USAGE           "2.5.29.37"
#define OID_STRING_EXTENDED_KEY_USAGE         "extendedKeyUsage"
#define OID_NAME_PIV_FASC_N                   "2.16.840.1.101.3.6.6"
#define OID_STRING_PIV_FASC_N                 "pivFASC-N"
#define OID_NAME_ID_PIV_CONTENT_SIGNING       "2.16.840.1.101.3.6.7"
#define OID_STRING_ID_PIV_CONTENT_SIGNING     "id-PIV-content-signing"
#define OID_NAME_NETSCAPECERTEXTN             "2.16.840.1.113730.1.1"
#define OID_STRING_NETSCAPECERTEXTN           "netscapeCertificateExtension"
#define OID_NAME_NETSCAPECERTCOMMENT          "2.16.840.1.113730.1.13"
#define OID_STRING_NETSCAPECERTCOMMENT        "netscapeCertificateComment"


// Other cert telemetry definitions...

#define TELE_CERT_EXTN_MAX (32)
#define OF_INVENTORY (2)

typedef struct tele_cert_extension_list
{
  char
    name [1024];
  char
    presence [1024];
} TELE_CERT_EXTENSION_LIST;

typedef struct tele_cert_context
{
  int
    verbosity;
  int
    output_format;
  char
    cert_file_base [1024];
  char
    cert_filename [1024];
  int
    extension_unknown;
  TELE_CERT_EXTENSION_LIST
    extensions [TELE_CERT_EXTN_MAX];
} TELE_CERT_CONTEXT;


#define ST_TELE_CERT_UNK_EXTN (100+0)


int
  add_extension_list_entry
    (TELE_CERT_CONTEXT
      *ctx,
    char
      *extension);
int
  init_tele_cert
    (TELE_CERT_CONTEXT
      *ctx);
int
  tele_cert_display_extensions
    (TELE_CERT_CONTEXT
      *ctx,
    gnutls_x509_crt_t
      cert);
char
  *tele_cert_extension_inventory
    (TELE_CERT_CONTEXT
      *ctx);
int
  tele_display_subject_alt_name
    (TELE_CERT_CONTEXT
      *ctx,
    gnutls_x509_crt_t
      cert,
    int
      *san_entry_count,
    char
      *san_inventory,
    int
      *san_inventory_length);
int
  tele_jacket_append
    (TELE_CERT_CONTEXT
      *ctx,
    char
      *parameter_name,
    char
      *parameter_value);

