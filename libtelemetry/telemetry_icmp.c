/*
  telemetry_icmp - ICMP Ping and low level packet routines.

  (C)Copyright 2017-2018 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

int icmp_id = 0xfeca;
int icmp_sequence = 0;
/*
  Wrapper only is "new", the main body of the code comes from

https://cboard.cprogramming.com/networking-device-communication/41635-ping-program.html

  Timeout code from

http://stackoverflow.com/questions/13547721/udp-socket-set-timeout

  Thank you to those contributors for doing the hard bits of handling the
  packet header and talking to Linux at such a low level.
*/

/*
 *    pinger.c 
 *    This is a ping imitation program 
 *    It will send an ICMP ECHO packet to the server of 
 *    your choice and listen for an ICMP REPLY packet
 *    Have fun!
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <linux/ip.h>
#include <linux/icmp.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


#include <gnutls/gnutls.h>
#include <libtelemetry.h>
#define TLM_PING4_DEFAULT_TTL (255)
 
 
char dst_addr[1024];
char src_addr[1024];
 
unsigned short in_cksum(unsigned short *, int);
void parse_argvs(char**, char*, char* );
void usage();
char* getip();
 

void
  dump_buffer
    (char
      *tag,
    char
      *p,
    int
      lth,
    int
      options)
{
  int i;
  fprintf (stderr, "->%s<-\n  %08lx:", tag, (unsigned long int)p);
  for (i=0; i<lth; i++)
  {
    fprintf (stderr, " %02x",
      *(i+(unsigned char *)p));
  };
  fprintf (stderr, "\n");
}

/*
  ping_ipv4 (context)

  use:
    fqdn (for now) must be an explicit dotted quad ipv4 address.
    this must be run as root.
 
  context is from libtelemetry.h.  use destination.  set source to "0.0.0.0"
    set parameter_1 to default TTL (original code used 255, 0 causes lib to use a default
      of 255
    response will contain ping response unless error.

  returned status:
    ok if it responded.
    ST_TLM_PING4_TIMEOUT if it timed out (ttl unchanged in response.)
    ST_TLM_PING4_zzz if some other recvfrom issue.
    response_contents_length and response_contents set.
*/
int
  ping_ipv4
    (TLM_PDU_CONTEXT
      *ctx)

{ /* ping_ipv4 */

    struct iphdr* ip_reply;
    struct icmphdr* icmp;
    struct sockaddr_in connection;
    char* packet;
    char* buffer;
    int sockfd;
  int
    addrlen;
  int
    buffer_returned_length;
  struct iphdr
    *ip;
  int
    optval;
  int
    packet_length;
  int
    rec_size; // actual size received
  int
    status;
  int
    status_socket;


  status = ST_OK;
  errno = 0;
  if (getuid () != 0)
  {
    fprintf (stderr, "ping_ipv4: root privelidges needed to access %s\n", ctx->destination);
    status = ST_TLM_NEEDS_ROOT;
  };
 
  if (status EQUALS ST_OK)
  {
    strcpy (dst_addr, ctx->destination);
    strcpy (src_addr, ctx->source); // recommend "0.0.0.0" for this value
    if (ctx->parameter_1 EQUALS 0)
      ctx->parameter_1 = TLM_PING4_DEFAULT_TTL;

    
    /*
     * allocate all necessary memory
    */
//    ip = malloc(sizeof(struct iphdr));
//    ip_reply = malloc(sizeof(struct iphdr));
//    icmp = malloc(sizeof(struct icmphdr));
    packet_length = sizeof(struct iphdr) + sizeof(struct icmphdr) + 64;
    packet = malloc(packet_length);
    memset (packet, 0, packet_length);
//    strcpy (packet+sizeof (struct iphdr) + sizeof (struct icmphdr), "enterprises.36169");
    buffer_returned_length =
      sizeof(struct iphdr) + sizeof(struct icmphdr) + 1024;
    buffer = malloc(buffer_returned_length);
    /****************************************************************/
     
    ip = (struct iphdr*) packet;
    icmp = (struct icmphdr*) (packet + sizeof(struct iphdr));
     
    /*  
     *  here the ip packet is set up except checksum
     */
    memset ((char *)ip, 0, sizeof (*ip));
    ip->ihl      = 5;
    ip->version  = 4;
    ip->tos      = 0;
    ip->tot_len  = 64+ sizeof(struct iphdr) + sizeof(struct icmphdr);
    ip->id       = htons(random());
    ip->ttl      = ctx->parameter_1;
    ip->protocol = IPPROTO_ICMP;
    ip->saddr            = inet_addr(src_addr);
    ip->daddr            = inet_addr(dst_addr);

    if ((sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) == -1)
    {
      status = ST_TLM_PING4_NET_1;
      perror("socket");
    }
  };
 
  if (status EQUALS ST_OK)
  {
    /* 
     *  IP_HDRINCL must be set on the socket so that
     *  the kernel does not attempt to automatically add
     *  a default ip header to the packet
     */
     
    optval=1; // to enable
    status_socket =
      setsockopt(sockfd, IPPROTO_IP, IP_HDRINCL, &optval, sizeof(int));

    /*
      set a timeout so it doesn't hang forever if the host isn't there.
    */
    if (status_socket EQUALS 0)
    {
      struct timeval
        timeout_time;

      timeout_time.tv_sec = 5;
      timeout_time.tv_usec = 0;
      status_socket = setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO,
        &timeout_time, sizeof (timeout_time));
      if (status_socket != 0)
      {
        fprintf (stderr, "status from setsockopt-timeout was %d\n",
          status_socket);
        status = ST_TLM_PING4_NET_2;
      };
    };
  };
 
  if (status EQUALS ST_OK)
  {
    /*
     *  here the icmp packet is created
     *  also the ip checksum is generated
     */
    icmp->type           = ICMP_ECHO;
    icmp->code           = 0;
    icmp->un.echo.id     = icmp_id;
icmp_sequence ++;
    icmp->un.echo.sequence   = htons (icmp_sequence);
    icmp->checksum       = 0;
    icmp-> checksum      = in_cksum((unsigned short *)icmp, packet_length-sizeof (struct iphdr));
     
    ip->check            = in_cksum((unsigned short *)ip, packet_length);
     
    connection.sin_family = AF_INET;
    connection.sin_addr.s_addr = inet_addr(dst_addr);
     
    /*
     *  now the packet is sent
     */
     
    sendto(sockfd, packet, ip->tot_len, 0, (struct sockaddr *)&connection, sizeof(struct sockaddr));
     
    /*
     *  now we listen for responses
     */
    addrlen = sizeof(connection);
    rec_size = recvfrom (sockfd, buffer, buffer_returned_length,
      0, (struct sockaddr *)&connection, (socklen_t *)&addrlen);
    if (rec_size EQUALS -1)
    {
      status = ST_TLM_PING4_NET_3;
      if (ctx->verbosity >3)
        fprintf (stderr, "recvfrom %d errno %d\n",
          rec_size, errno);
    }
  };

  if (status EQUALS ST_OK)
  {
    memcpy (ctx->results, buffer, rec_size);
    ctx->results_length = rec_size;
    ip_reply = (struct iphdr*) buffer;
    ctx->indicator_1 = ntohs(ip_reply->id);
    ctx->indicator_2 = ip_reply->ttl;

    if (ip_reply->ttl EQUALS ctx->parameter_1) // in other words TTL never moved from initial value
      status = ST_TLM_PING4_TIMEOUT;
  }; /* ok after root check */

  // unless the socket call went sideways, close the socket file descriptor

  if (status != ST_TLM_PING4_NET_1)
    close (sockfd);

  return (status);
}
 

char* getip()
{
    char buffer[256];
    struct hostent* h;
     
    gethostname(buffer, 256);
    h = gethostbyname(buffer);
     
    return inet_ntoa(*(struct in_addr *)h->h_addr);
     
}
/*
 * in_cksum --
 * Checksum routine for Internet Protocol
 * family headers (C Version)
 */
unsigned short in_cksum(unsigned short *addr, int len)
{
    register int sum = 0;
    u_short answer = 0;
    register u_short *w = addr;
    register int nleft = len;
    /*
     * Our algorithm is simple, using a 32 bit accumulator (sum), we add
     * sequential 16 bit words to it, and at the end, fold back all the
     * carry bits from the top 16 bits into the lower 16 bits.
     */
    while (nleft > 1)
    {
      sum += *w++;
      nleft -= 2;
    }
    /* mop up an odd byte, if necessary */
    if (nleft == 1)
    {
      *(u_char *) (&answer) = *(u_char *) w;
      sum += answer;
    }
    /* add back carry outs from top 16 bits to low 16 bits */
    sum = (sum >> 16) + (sum & 0xffff);       /* add hi 16 to low 16 */
    sum += (sum >> 16);               /* add carry */
    answer = ~sum;              /* truncate to 16 bits */
    return (answer);
}

