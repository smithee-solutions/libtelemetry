#if 0
libtelemetry
  formats
    cef
    snmp
#endif
/*
  uses libtelemetry (github: smithee-us/libtelemetry)

  (C)Copyright 2017-2020 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/


#include <stdio.h>
#include <memory.h>

#include <jansson.h>

#include <gnutls/gnutls.h>


#include <libtelemetry.h>


int
  tlm_init
    (TLM_CONTEXT
      *ctx)
{ /* tlm_init */

  FILE
    *cfgf;
  char
    field [1024];
  int
    found_field;
  char
    json_string [1024];
  json_t
    *root;
  int
    status;
  int
    status_io;
  json_error_t
    status_json;
  json_t
    *value;
  char
    vstr [1024];


  status = ST_TLM_CFG_READ;
  memset (ctx, 0, sizeof (*ctx));
  ctx->verbosity = 1;
  cfgf = fopen ("/opt/tester/etc/libtelemetry_config.json", "r");
  if (cfgf != NULL)
  {
    status = ST_OK;
    memset (json_string, 0, sizeof (json_string));
    status_io = fread (json_string, sizeof (json_string[0]), sizeof (json_string), cfgf);
    if (status_io > sizeof (json_string))
      status = ST_TLM_CFG_OVERFLOW;
    if (status_io < 0)
      status = ST_TLM_CFG_UNDERFLOW;
  };
  if (status EQUALS ST_OK)
  {
    root = json_loads (json_string, 0, &status_json);
    if (!root)
    {
      fprintf (stderr, "JSON parser failed.  String was ->%s<-\n",
        json_string);
      status = ST_TLM_CFG_ERROR;
    };
  };

  // set results-path where output goes

  if (status EQUALS ST_OK)
  {
    found_field = 1;
    strcpy (field, "results-path");
    value = json_object_get (root, field);
    if (!json_is_string (value))
      found_field = 0;
    if (found_field)
    {
      strcpy(ctx->results_path, json_string_value(value));
    };
  };

  // set verbosity=n where n is typically 0..9

  if (status EQUALS ST_OK)
  {
    found_field = 1;
    strcpy (field, "verbosity");
    value = json_object_get (root, field);
    if (!json_is_string (value))
      found_field = 0;
    if (found_field)
    {
      int i;
      strcpy (vstr, json_string_value (value));
      sscanf (vstr, "%d", &i);
      ctx->verbosity = i;
    };
  };

  // set verify_server=enable or disable

  if (status EQUALS ST_OK)
  {
    found_field = 1;
    strcpy (field, "verify_server");
    value = json_object_get (root, field);
    if (!json_is_string (value))
      found_field = 0;
    if (found_field)
    {
      strcpy (vstr, json_string_value (value));
      if (0 EQUALS strcmp ("enable", vstr))
        ctx->verify_server = 1;
    };
  };

  return (status);
}

int
  tlm_recorder_set
    (TLM_STREAM
      *recorder,
    int
      tlm_set_cmd,
    void
      *tlm_set_value1)

{ /* tlm_recorder_set */

  int
    status;
  char
    tstring [1024];


  status = ST_OK;
  switch (tlm_set_cmd)
  {
  case TLM_START_RECORDER:
    recorder->file = fopen (recorder->recorder_path, "w");
    if (recorder->file EQUALS NULL)
      status = ST_TLM_REC_BAD_PATH;
    break;
  case TLM_STREAM_FILENAME:
    strcpy (recorder->recorder_path, (char *)tlm_set_value1);
    break;
  case TLM_STREAM_FORMAT:
    recorder->stream_format = (UINT32)tlm_set_value1;
    switch (recorder->stream_format)
    {
    case TLM_FMT_TLS:
      memset (&(recorder->tls_telemetry), 0, sizeof (recorder->tls_telemetry));
      sprintf (tstring, "%s/recorder_%02d_telemetry.json",
        recorder->recorder_path, recorder->stream_id);
      strcpy (recorder->recorder_path, tstring);
      break;
    };
    break;
  default:
    status = ST_TLM_SET_UNKNOWN;
    break;
  };
  return (status);

} /* tlm_recorder_set */


int
  tlm_recorder_action
  (TLM_STREAM
    *recorder,
  int
    stop_command)
{ /* tlm_recorder_action */

  int status;
  status = ST_OK;
  if (recorder->file EQUALS NULL)
    status = ST_TLM_BAD_RECORDER;
  if (status EQUALS ST_OK)
  {
    if (stop_command == TLM_STREAM_FLUSH)
      fflush (recorder->file);
    if (stop_command == TLM_STREAM_CLOSE)
      fclose (recorder->file);
  };
  return (status);
} /* tlm_recorder_action */


int
  tlm_recorder_init
    (TLM_CONTEXT *tctx,
    TLM_STREAM *recorder,
    int record_command,
    int new_stream_id)

{ /* tlm_recorder_init */

  int
    status;


  status = ST_STREAM_ERROR;
  memset ((char *)recorder, 0, sizeof (*recorder));
  recorder->stream_id = new_stream_id;
  if (tctx->verbosity > 3)
    fprintf (stderr, "tlm debug: tlm_recorder_init cmd %d\n",
      record_command);
  if (record_command EQUALS TLM_STREAM_INIT)
  {
    if (tctx->verbosity > 3)
      fprintf (stderr, "Telemetry: Initializing stream %04d\n",
        recorder->stream_id);
    status = ST_OK;
  };
  if (record_command EQUALS TLM_STREAM_ENGAGE)
  {
    if (tctx->verbosity > 3)
    {
      fprintf (stderr, "Telemetry: Initializing and recording: stream %04d\n",
        recorder->stream_id);
      fprintf (stderr, "Telemetry: Recorder path %s\n",
        recorder->recorder_path);
    };
    status = ST_OK;
    recorder->file = fopen (recorder->recorder_path, "w");
    if (recorder->file EQUALS NULL)
      status = ST_BAD_REC_ENGAGE;
  };
  return (status);

} /* tlm_recorder_init */

