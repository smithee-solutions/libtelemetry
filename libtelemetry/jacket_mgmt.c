/*
  (C)Copyright 2017-2018 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

//  "ranking" : "000000001",
//"name" : "cqure.pl"
/*
  manage jackets.  used by update_jacket
*/

#include <stdio.h>
#include <string.h>


#include <jansson.h>
#include <gnutls/gnutls.h>


#include <libtelemetry.h>

extern int
  verbosity;

int
  update_jacket
    (TLM_JACKET
      *jacket,
    char
      *parameter,
    char
      *value);


int
  read_jacket
    (char
      *jacket_name,
    TLM_JACKET
      *jacket)

{ /* read_jacket */

  json_t
    *array_entry;
  json_t
    *array_object;
  char
    field [1024];
  int
    i;
  FILE
    *jf;
  char
    json_string [1024];
  char
    *ptr;
  json_t
    *root;
  int
    status;
  int
    status_io;
  json_error_t
    status_json;
  json_t
    *value_object;


  status = ST_OK;
  memset (jacket, 0, sizeof (*jacket));
  jf = fopen (jacket_name, "r");
  if (jf EQUALS NULL)
    status = ST_BAD_JACKET;
  if (status EQUALS ST_OK)
  {
    memset (json_string, 0, sizeof (json_string));
    status_io = fread (json_string,
      sizeof (json_string [0]), sizeof (json_string), jf);

    if (status_io >= sizeof (json_string))
      status = ST_JACKET_OVERFLOW;
    if (status_io <= 0)
      status = ST_JACKET_UNDERFLOW;
  };
  if (status EQUALS ST_OK)
  {
    root = json_loads (json_string, 0, &status_json);
    if (!root)
    {
      fprintf (stderr, "JSON parser failed.  String was ->\n%s<-\n",
        json_string);
      status = ST_JACKET_PARSE;
    };
  };
  if (status EQUALS ST_OK) {
    strcpy (field, "_comment"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_COMMENT;
    if (status EQUALS ST_OK) strcpy (jacket->comment, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "dsap_0"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_DSAP0;
    if (status EQUALS ST_OK) strcpy (jacket->dsap_0, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "dsap_1"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_DSAP1;
    if (status EQUALS ST_OK) strcpy (jacket->dsap_1, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "dsap_2"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_DSAP2;
    if (status EQUALS ST_OK) strcpy (jacket->dsap_2, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "fqdn"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_FQDN;
    if (status EQUALS ST_OK) strcpy (jacket->fqdn, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "name"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_NAME;
    if (status EQUALS ST_OK) strcpy (jacket->name, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "ranking"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_RANKING;
    if (status EQUALS ST_OK) strcpy (jacket->ranking, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "software_version"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_SWVERS;
    if (status EQUALS ST_OK) strcpy (jacket->software_version, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "tag"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_TAG;
    if (status EQUALS ST_OK) strcpy (jacket->tag, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK)
  {
    strcpy (field, "ipv4");
    value_object = json_object_get (root, field);
    if (!json_is_string (value_object))
      status = ST_JACKET_PARSE_IPV4;
    if (status EQUALS ST_OK)
      strcpy (jacket->ipv4, json_string_value (value_object));
    status = ST_OK;
  };
  if (status EQUALS ST_OK)
  {
    strcpy (field, "mac");
    value_object = json_object_get (root, field);
    if (!json_is_string (value_object))
      status = ST_JACKET_PARSE_MAC;
    if (status EQUALS ST_OK)
      strcpy (jacket->mac, json_string_value (value_object));
    status = ST_OK;
  };
  if (status EQUALS ST_OK) {
    strcpy (field, "model"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_VENDOR;
    if (status EQUALS ST_OK) strcpy (jacket->model, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "vendor"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_VENDOR;
    if (status EQUALS ST_OK) strcpy (jacket->vendor, json_string_value (value_object));
    status = ST_OK; };
  if (status EQUALS ST_OK) {
    strcpy (field, "version"); value_object = json_object_get (root, field);
    if (!json_is_string (value_object)) status = ST_JACKET_PARSE_VERSION;
    if (status EQUALS ST_OK) strcpy (jacket->version, json_string_value (value_object));
    status = ST_OK;
  };
  if (status EQUALS ST_OK)
  {
    strcpy (field, "protocols");
    array_object = json_object_get (root, field);
    if (!json_is_array (array_object))
      status = ST_JACKET_PARSE_PROTOCOLS;
    if (status EQUALS ST_OK)
    {
      int proto_list_size;

      proto_list_size= json_array_size (array_object);
      jacket->proto_list_size = proto_list_size;
      if (proto_list_size > 2)
status = -1;
      if (status EQUALS ST_OK)
      {
        for (i=0; i<proto_list_size; i++)
        {
          array_entry = json_array_get (array_object, i);
          strcpy (field, "proto");
          value_object = json_object_get (array_entry, field);
          ptr = jacket->proto_list + (i*32);
          strcpy (ptr, json_string_value (value_object));
          fprintf (stderr, "proto %s\n", json_string_value (value_object));
        };
      };
    };
    status = ST_OK;
  };
  if (jf != NULL)
  {
    json_decref (root);
    fclose (jf);
  };

  return (status);

} /* read_jacket */


int
  update_jacket
    (TLM_JACKET
      *jacket,
    char
      *parameter,
    char
      *value)

{ /* update_jacket */

  int
    found;
  int
    status;


  status = ST_OK;
// fqdn ipv4 mac vendor protocols

  // try them all.  'found' will get set if we have a hit otherwise report error
  found = 0;
  if (0 == strcmp ("_comment", parameter))
  {
    strcpy (jacket->comment, value);
    found = 1;
  };
  if (0 == strcmp ("fqdn", parameter))
  {
    strcpy (jacket->fqdn, value);
    found = 1;
  };
  if (0 == strcmp ("ipv4", parameter))
  {
    strcpy (jacket->ipv4, value);
    found = 1;
  };
  if (0 == strcmp ("mac", parameter))
  {
    strcpy (jacket->mac, value);
    found = 1;
  };
  if (0 == strcmp ("model", parameter))
  {
    strcpy (jacket->model, value);
    found = 1;
  };
//name
  if (0 == strcmp ("name", parameter))
  {
    strcpy (jacket->name, value);
    found = 1;
  };
//ranking
  if (0 == strcmp ("ranking", parameter))
  {
    strcpy (jacket->ranking, value);
    found = 1;
  };
  if (0 == strcmp ("software_version", parameter))
  {
    strcpy (jacket->software_version, value);
    found = 1;
  };
  if (0 == strcmp ("tag", parameter))
  {
    strcpy (jacket->tag, value);
    found = 1;
  };
  if (0 == strcmp ("vendor", parameter))
  {
    strcpy (jacket->vendor, value);
    found = 1;
  };
  if (0 == strcmp ("version", parameter))
  {
    strcpy (jacket->version, value);
    found = 1;
  };
  /*
    skip "protocols" as that's an array.
  */
    /*
      remember protocols is an array.  it looks like this:

      "protocols" : [{"proto":"tcp"},{"proto":"udp"}]
    */

  if (!found)
    status = ST_JACKET_PARAM_UNKNOWN;
  return (status);

} /* update_jacket */


/*
  create_jacket - create a new jacket

  usage:

    call with populated jacket structure, name of jacket,
    jacket format.  Format is "ipv4" or "fqdn" at this time.

    this assumes you are in the collection directory.
*/

int
  create_jacket
  (TLM_JACKET
     *jacket,
  char
    *entry_name,
  char
    *jacket_format)

{ /* create_jacket */

  char
    initial_jacket_entry [1024];
  FILE
    *irepf;
  char
    irep_filename [1024];
  TLM_JACKET
    old_jacket;
  int
    status;


  status = ST_OK;
  strcpy (initial_jacket_entry, "{ \"version\" : \"0\" }");
  status = ST_JACKET_FMT_UNK;
  if (0 EQUALS strcmp (jacket_format, "ipv4"))
  {
    status = ST_JACKET_FMT_UNIMP;
  };
  if (0 EQUALS strcmp (jacket_format, "fqdn"))
  {
    sprintf (initial_jacket_entry, "{ \"version\" : \"0\", \"%s\" : \"%s\", \"_end\": \"end of jacket\"}",
      TLM_RM_TAG_FQDN, jacket->fqdn);
    status = validate_jacket_entry_name (jacket_format, entry_name);
    if (status EQUALS ST_OK)
    {
      sprintf (irep_filename, "jacket_%s.json",
        entry_name);
      status = read_jacket (irep_filename, &old_jacket);
      if (status != ST_BAD_JACKET)
      {
        status = ST_OK;
        if (verbosity >3)
          fprintf (stderr, "entry filename will be %s\n",
            irep_filename);
        irepf = fopen (irep_filename, "w");
        if (irepf != NULL)
        {
          if (verbosity >3)
            fprintf (stderr, "writing file now\n");
          fprintf (irepf, "{\n%s", initial_jacket_entry);
          if (irepf != NULL)
            fclose (irepf);
        }
        else
        {
          status = ST_JACKET_WRITE;
        };
      };
    };
  };
  return (status);

} /* create_jacket */


int
  validate_jacket_entry_name
    (char * jacket_format,
    char * entry_name)

{
  int status;

  status = -1;
  if (0 EQUALS strcmp (jacket_format, "fqdn"))
    status = ST_OK;
  return (status);
}

